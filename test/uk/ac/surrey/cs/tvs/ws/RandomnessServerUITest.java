/*******************************************************************************
* Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ws;

import static org.junit.Assert.assertNotNull;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.NotYetConnectedException;

import org.java_websocket.WebSocket;
import org.java_websocket.drafts.Draft;
import org.java_websocket.exceptions.InvalidDataException;
import org.java_websocket.exceptions.InvalidHandshakeException;
import org.java_websocket.framing.Framedata;
import org.java_websocket.handshake.ClientHandshakeBuilder;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>VECWebSocketTest</code> contains tests for the class <code>{@link RandomnessServerUI}</code>.
 */
public class RandomnessServerUITest {

  /**
   * Dummy web socket class. Does nothing.
   */
  private class DummyWebSocket extends WebSocket {

    @Override
    public void close(int arg0) {
    }

    @Override
    public void close(int arg0, String arg1) {
    }

    @Override
    protected void close(InvalidDataException arg0) {
    }

    @Override
    public Draft getDraft() {
      return null;
    }

    @Override
    public InetSocketAddress getLocalSocketAddress() {
      return null;
    }

    @Override
    public int getReadyState() {
      return 0;
    }

    @Override
    public InetSocketAddress getRemoteSocketAddress() {
      return null;
    }

    @Override
    public boolean hasBufferedData() {
      return false;
    }

    @Override
    public boolean isClosed() {
      return false;
    }

    @Override
    public boolean isClosing() {
      return false;
    }

    @Override
    public boolean isConnecting() {
      return false;
    }

    @Override
    public boolean isOpen() {
      return false;
    }

    @Override
    public void send(byte[] arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(ByteBuffer arg0) throws IllegalArgumentException, NotYetConnectedException {
    }

    @Override
    public void send(String arg0) throws NotYetConnectedException {
    }

    @Override
    public void sendFrame(Framedata arg0) {
    }

    @Override
    public void startHandshake(ClientHandshakeBuilder arg0) throws InvalidHandshakeException {
    }
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    String[] args = new String[] {};
    
    RandomnessServerUI.main(args);
    
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    String[] args = new String[] { TestParameters.CONFIG_FILE };
    
    RandomnessServerUI.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_3() throws Exception {
    String[] args = new String[] { "rubbish", "rubbish" };
    Thread.sleep(2000);
    RandomnessServerUI.main(args);
  }

  /**
   * Run the RandomnessServerUI(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRandomnessServerUI_1() throws Exception {
    
    RandomnessServerUI fixture = new RandomnessServerUI(TestParameters.CONFIG_FILE);
    assertNotNull(fixture);

    WebSocket ws = new DummyWebSocket();
    JSONObject message = new JSONObject();

    message.put("type", "genRandomness");

    fixture.processMessage(message, ws);

    message.put("type", "getFailures");

    fixture.processMessage(message, ws);
    fixture.shutdown();
  }
}