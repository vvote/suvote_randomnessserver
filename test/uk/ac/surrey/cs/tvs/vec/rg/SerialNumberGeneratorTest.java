/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>SerialNumberGeneratorTest</code> contains tests for the class <code>{@link SerialNumberGenerator}</code>.
 */
public class SerialNumberGeneratorTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();

    // Create the output folder.
    new File(TestParameters.OUTPUT_FOLDER).mkdirs();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void generateSerialNumbers(int,String,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateSerialNumbers_1() throws Exception {
    // Generate the serial number file.
    SerialNumberGenerator.generateSerialNumbers(TestParameters.ROWS, TestParameters.OUTPUT_SERIAL_DATA_FILE, TestParameters.PREFIX);

    // Now verify that it has the correct content.
    int serialCount = 0;

    BufferedReader serialReader = null;

    try {
      serialReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_SERIAL_DATA_FILE));

      String serialLine = serialReader.readLine();

      while (serialLine != null) {
        serialCount++;

        // Convert the lines into JSON objects and check their content.
        JSONObject serial = new JSONObject(serialLine);
        assertTrue(serial.has("serialNo"));

        String serialNo = (String) serial.get("serialNo");
        assertTrue(serialNo.startsWith(TestParameters.PREFIX));

        int value = Integer.parseInt(serialNo.substring(TestParameters.PREFIX.length() + 1)); // Also remove ":".
        assertEquals(serialCount, value);

        serialLine = serialReader.readLine();
      }
    }
    // Pass all exceptions up.
    finally {
      if (serialReader != null) {
        serialReader.close();
      }
    }

    // Check the number of objects.
    assertEquals(TestParameters.ROWS, serialCount);

    // Tidy up the file.
    new File(TestParameters.OUTPUT_SERIAL_DATA_FILE).delete();
  }
}