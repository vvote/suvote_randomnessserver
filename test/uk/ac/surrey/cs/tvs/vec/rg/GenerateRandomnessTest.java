/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;

/**
 * The class <code>GenerateRandomnessTest</code> contains tests for the class <code>{@link GenerateRandomness}</code>.
 */
public class GenerateRandomnessTest {

  /** Tracks progress. */
  private int progress = 0;

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void addProgressListener(ProgressListener) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddProgressListener_1() throws Exception {
    GenerateRandomness fixture = new GenerateRandomness(TestParameters.getInstance().getPublicKey(), TestParameters.COLUMNS,
        TestParameters.ROWS, TestParameters.BIT_SIZE, TestParameters.OUTPUT_RANDOM_DATA_FILE,
        TestParameters.OUTPUT_COMMIT_DATA_FILE);

    // Test adding a progress listener.
    ProgressListener listener = new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        GenerateRandomnessTest.this.progress = value;
      }
    };

    fixture.addProgressListener(listener);

    // Test incrementing progress - with listener update.
    this.progress = 0;
    fixture.incrementProgress(1);
    assertEquals(1, this.progress);

    // Test incrementing progress - without listener update.
    fixture.incrementProgress(0);
    assertEquals(1, this.progress);

    // Test removing a progress listener.
    fixture.removeProgressListener(listener);
  }

  /**
   * Run the void generate(String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerate_1() throws Exception {
    GenerateRandomness fixture = new GenerateRandomness(TestParameters.getInstance().getPublicKey(), TestParameters.COLUMNS,
        TestParameters.ROWS, TestParameters.BIT_SIZE, TestParameters.OUTPUT_RANDOM_DATA_FILE,
        TestParameters.OUTPUT_COMMIT_DATA_FILE);

    // Delete any old versions of the output files.
    new File(TestParameters.OUTPUT_RANDOM_DATA_FILE).delete();
    new File(TestParameters.OUTPUT_COMMIT_DATA_FILE).delete();

    // Generate the random and commit files.
    File serialFile = TestParameters.getInstance().generateSerialNumberFile(TestParameters.ROWS);
    fixture.generate(serialFile.getAbsolutePath());

    // Test that each file has the correct number of content. There should be one row per serial number.
    int randomCount = 0;
    int commitCount = 0;

    BufferedReader serialReader = null;
    BufferedReader randomReader = null;
    BufferedReader commitReader = null;

    try {
      serialReader = new BufferedReader(new FileReader(serialFile));
      randomReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_RANDOM_DATA_FILE));
      commitReader = new BufferedReader(new FileReader(TestParameters.OUTPUT_COMMIT_DATA_FILE));

      String serialLine = serialReader.readLine();
      String randomLine = randomReader.readLine();
      String commitLine = commitReader.readLine();

      while ((serialLine != null) && (randomLine != null) && (commitLine != null)) {
        randomCount++;
        commitCount++;

        // Convert the lines into JSON objects.
        JSONObject serialObject = new JSONObject(serialLine);
        JSONObject randomObject = new JSONObject(randomLine);
        JSONObject commitObject = new JSONObject(commitLine);

        // Check the content.
        assertTrue(randomObject.has("serialNo"));
        assertTrue(randomObject.has("randomness"));

        assertTrue(commitObject.has("serialNo"));
        assertTrue(commitObject.has("randomness"));

        assertEquals(serialObject.get("serialNo"), randomObject.get("serialNo"));
        assertEquals(serialObject.get("serialNo"), commitObject.get("serialNo"));

        JSONArray randomArray = (JSONArray) randomObject.get("randomness");
        assertEquals(TestParameters.COLUMNS, randomArray.length());

        JSONArray commitArray = (JSONArray) commitObject.get("randomness");
        assertEquals(TestParameters.COLUMNS, commitArray.length());

        // Note that the detailed content of the files is tested in RandomnessWorkerTest.

        serialLine = serialReader.readLine();
        randomLine = randomReader.readLine();
        commitLine = commitReader.readLine();
      }
    }
    // Pass all exceptions up.
    finally {
      if (serialReader != null) {
        serialReader.close();
      }
      if (randomReader != null) {
        randomReader.close();
      }
      if (commitReader != null) {
        commitReader.close();
      }
    }

    // Check the number of objects.
    assertEquals(TestParameters.ROWS, randomCount);
    assertEquals(TestParameters.ROWS, commitCount);

    // Tidy up.
    new File(TestParameters.OUTPUT_RANDOM_DATA_FILE).delete();
    new File(TestParameters.OUTPUT_COMMIT_DATA_FILE).delete();
  }

  /**
   * Run the GenerateRandomness(PublicKey,int,int,int,String,String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateRandomness_1() throws Exception {
    GenerateRandomness result = new GenerateRandomness(TestParameters.getInstance().getPublicKey(), TestParameters.COLUMNS,
        TestParameters.ROWS, TestParameters.BIT_SIZE, TestParameters.OUTPUT_RANDOM_DATA_FILE,
        TestParameters.OUTPUT_COMMIT_DATA_FILE);

    assertNotNull(result);
  }

  /**
   * Run the byte[] getEncryptedKey() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGetEncryptedKey_1() throws Exception {
    GenerateRandomness fixture = new GenerateRandomness(TestParameters.getInstance().getPublicKey(), TestParameters.COLUMNS,
        TestParameters.ROWS, TestParameters.BIT_SIZE, TestParameters.OUTPUT_RANDOM_DATA_FILE,
        TestParameters.OUTPUT_COMMIT_DATA_FILE);

    // Test that the encrypted key is really the encrypted version of a secret key generated in the generate method.
    assertNull(fixture.getEncryptedKey());

    File serialFile = TestParameters.getInstance().generateSerialNumberFile(TestParameters.ROWS);
    fixture.generate(serialFile.getAbsolutePath());

    assertNotNull(fixture.getEncryptedKey());
  }
}