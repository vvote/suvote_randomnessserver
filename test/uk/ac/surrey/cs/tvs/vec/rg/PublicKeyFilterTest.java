/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import static org.junit.Assert.assertEquals;

import java.io.File;

import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;

/**
 * The class <code>PublicKeyFilterTest</code> contains tests for the class <code>{@link PublicKeyFilter}</code>.
 */
public class PublicKeyFilterTest {

  /**
   * Run the boolean accept(File,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAccept_1() throws Exception {
    PublicKeyFilter fixture = new PublicKeyFilter();
    File file = new File("");
    String name = TestParameters.PUBLIC_KEY_FILE;

    boolean result = fixture.accept(file, name);

    assertEquals(true, result);
  }

  /**
   * Run the boolean accept(File,String) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAccept_2() throws Exception {
    PublicKeyFilter fixture = new PublicKeyFilter();
    File file = new File("");
    String name = "rubbish";

    boolean result = fixture.accept(file, name);

    assertEquals(false, result);
  }
}