/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocket;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.JSONConstants;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>ProcessKeyFilesTest</code> contains tests for the class <code>{@link ProcessKeyFiles}</code>.
 */
public class ProcessKeyFilesTest {

  /**
   * Thread to process a message and a file.
   */
  private class DataThread extends Thread {

    /** The client socket. */
    private Socket     socket         = null;

    /** The message that has been read. */
    private JSONObject message        = null;

    /** The file data that has been read. */
    private byte[]     data           = null;

    /** Use a valid signature on response? */
    private boolean    validSignature = true;

    private int        peerid;

    /**
     * @param socket
     *          The client socket.
     * @param validSignature
     *          Use a valid signature on response?
     */
    public DataThread(Socket socket, boolean validSignature, int id) {
      super();

      this.socket = socket;
      this.validSignature = validSignature;
      this.peerid = id;
    }

    /**
     * @return A properly signed peer response.
     * @throws JSONException
     * @throws CryptoIOException
     * @throws TVSSignatureException
     */
    private JSONObject createResponse() throws JSONException, TVSSignatureException, CryptoIOException {
      String data = "rubbish";

      if (this.validSignature) {
        data = this.message.getString(FileMessage.ID);
        data = data + TestParameters.SOURCE_SERVER;
        data = data + TestParameters.DEVICE;
        data = data + this.message.getString(FileMessage.DIGEST);
        data = data + TestParameters.COMMIT_TIME;
      }

      String signature = TestParameters.signData(data, peerid);

      JSONObject response = new JSONObject();
      response.put(MessageFields.PeerResponse.PEER_SIG, signature);
      response.put(JSONConstants.Signature.SIGNER_ID, TestParameters.SIGNING_PEERS[peerid]);
      response.put(MessageFields.PeerResponse.COMMIT_TIME, TestParameters.COMMIT_TIME);
      response.put(MessageFields.TYPE, MessageFields.TYPE);

      return response;
    }

    /**
     * @return the data.
     */
    private byte[] getData() {
      return this.data;
    }

    /**
     * @return the message.
     */
    private JSONObject getMessage() {
      return this.message;
    }

    /**
     * Processes the data for a pretend MBB.
     * 
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      BufferedInputStream in = null;
      BufferedWriter out = null;

      try {
        // Suck all the data and save it away.
        in = new BufferedInputStream(this.socket.getInputStream());
        boolean readMessage = false;
        boolean finished = false;
        int readLength = 0;
        int count = 0;
        int b = in.read();

        while ((b != -1) && !finished) {
          count++;

          // We expect a message.
          if (!readMessage && (b == '\n')) {
            this.message = new JSONObject(buffer.toString());
            readLength = (int) this.message.get(FileMessage.FILESIZE);

            readMessage = true;
            buffer.reset();
          }

          // And then a file.
          if (readMessage && count >= readLength) {
            finished = true;
          }

          buffer.write(b);

          // Only read in the required amount of data.
          if (!finished) {
            b = in.read();
          }
        }

        this.data = buffer.toByteArray();

        // Send the response.
        out = new BufferedWriter(new OutputStreamWriter(this.socket.getOutputStream()));
        out.write(this.createResponse().toString());
        out.newLine();
        out.flush();
      }
      catch (IOException | JSONException e) {
        e.printStackTrace();
      }
      catch (TVSSignatureException e) {
        e.printStackTrace();
      }
      catch (CryptoIOException e) {
        e.printStackTrace();
      }
      finally {
        if (out != null) {
          try {
            out.close();
          }
          catch (IOException e) {
            e.printStackTrace();
          }
        }
        if (in != null) {
          try {
            in.close();
          }
          catch (IOException e) {
            e.printStackTrace();
          }
        }

        try {
          this.socket.close();
        }
        catch (IOException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /**
   * Class which pretends to be an MBB by listening on a port.
   */
  private class PretendMBBThread extends Thread {

    /** The port to listen on. */
    private int              port           = 0;

    /** The listening server socket. */
    private SSLServerSocket  serverSocket   = null;

    /** The data threads. */
    private List<DataThread> threads        = new ArrayList<DataThread>();

    /** Are we running? */
    private boolean          running        = true;

    /** Use a valid signature on response? */
    private boolean          validSignature = true;

    /**
     * Which peer is this
     */
    private int              id;

    /**
     * @param port
     *          The port to listen on.
     */
    public PretendMBBThread(int port, int id) {
      super();

      this.port = port;
      this.id = id;
    }

    /**
     * @return All of the sub-thread data.
     */
    public List<byte[]> getData() {
      List<byte[]> data = new ArrayList<byte[]>();

      for (DataThread thread : this.threads) {
        data.add(thread.getData());
      }

      return data;
    }

    /**
     * @return All of the sub-thread messages.
     */
    public List<JSONObject> getMessages() {
      List<JSONObject> messages = new ArrayList<JSONObject>();

      for (DataThread thread : this.threads) {
        messages.add(thread.getMessage());
      }

      return messages;
    }

    /**
     * Initialises and returns the SSL socket factory needed to create server SSL connections.
     * 
     * @return The SSL socket factory.
     * @throws CryptoIOException
     * @throws NoSuchAlgorithmException
     * @throws KeyStoreException
     * @throws UnrecoverableKeyException
     * @throws KeyManagementException
     */
    public SSLServerSocketFactory getServerSSLSocketFactory() throws CryptoIOException, NoSuchAlgorithmException,
        UnrecoverableKeyException, KeyStoreException, KeyManagementException {
      // Initialise the trust store for the server key.
      TrustManagerFactory tmf = null;
      KeyManagerFactory kmf = null;

      KeyStore keyStore = CryptoUtils.loadKeyStore(TestParameters.MBB_KEY_STORE, TestParameters.KEY_STORE_PASSWORD.toCharArray());

      tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
      tmf.init(keyStore);

      kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
      kmf.init(keyStore, TestParameters.KEY_STORE_PASSWORD.toCharArray());

      // Create the server socket factory using the trust store.
      SSLContext context = SSLContext.getInstance("TLS");

      context.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
      return context.getServerSocketFactory();
    }

    /**
     * Pretends to be the MBB by listening.
     * 
     * @see java.lang.Thread#run()
     */
    @Override
    public void run() {
      // Listen on the specified port and wait for a connection.
      try {
        this.serverSocket = (SSLServerSocket) this.getServerSSLSocketFactory().createServerSocket();
        this.serverSocket.setEnabledCipherSuites(TestParameters.CIPHERS);
        this.serverSocket.setNeedClientAuth(true);
        this.serverSocket.setUseClientMode(false);
        this.serverSocket.setReuseAddress(true);

        InetSocketAddress address = new InetSocketAddress(this.port);
        this.serverSocket.bind(address);

        while (this.running) {
          DataThread thread = new DataThread(this.serverSocket.accept(), this.validSignature, this.id);
          thread.start();
          this.threads.add(thread);
        }
      }
      catch (IOException | KeyManagementException | NoSuchAlgorithmException | UnrecoverableKeyException | KeyStoreException
          | CryptoIOException e) {
        // e.printStackTrace();
        // Ignore - we are expecting a socket error on close.
      }
      finally {
        if (this.serverSocket != null) {
          try {
            this.serverSocket.close();
          }
          catch (IOException e) {
            e.printStackTrace();
          }
        }
      }
    }

    /**
     * @param validSignature
     *          the validSignature to set
     */
    public void setValidSignature(boolean validSignature) {
      this.validSignature = validSignature;
    }

    /**
     * Shuts down listening.
     */
    public void shutdown() {
      this.running = false;
      try {
        this.serverSocket.close();
      }
      catch (IOException e1) {
        e1.printStackTrace();
      }

      // Wait for the sub-threads.
      for (Thread thread : this.threads) {
        try {
          thread.join();
        }
        catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }

  /** Tracks all progress values. */
  private List<Integer> progressValues = new ArrayList<Integer>();

  /** Tracks a single progress value. */
  private int           progress       = 0;

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();

    // Move all of the processed keys to the keys to process.
    File sourceFolder = new File(TestParameters.PROCESSED_KEYS_FILE_FOLDER);
    File destinationFolder = new File(TestParameters.KEYS_TO_PROCESS_FILE_FOLDER);

    destinationFolder.mkdirs();
    if (sourceFolder.exists()) {
      File[] files = sourceFolder.listFiles();

      for (int i = 0; i < files.length; i++) {
        try {
          IOUtils.moveFile(files[i].getAbsolutePath(), destinationFolder.getAbsolutePath());
        }
        catch (IOException e) {
          // file probably already exists, ignore
        }
      }
    }
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void addProgressListener(ProgressListener) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testAddProgressListener_1() throws Exception {
    ProcessKeyFiles fixture = new ProcessKeyFiles(TestParameters.KEYS_TO_PROCESS_FILE_FOLDER, new ConfigFile(
        TestParameters.CONFIG_FILE));

    // Test adding a progress listener.
    ProgressListener listener = new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        ProcessKeyFilesTest.this.progressValues.add(value);
      }
    };

    fixture.addProgressListener(listener);

    // Test getting progress.
    this.progressValues.clear();
    // Run some pretend MBBs to receive the data.
    int[] ports = new int[] { 9091, 9092, 9093 };
    List<PretendMBBThread> pretendMBBs = new ArrayList<PretendMBBThread>();

    for (int i = 0; i < ports.length; i++) {
      PretendMBBThread pretendMBB = new PretendMBBThread(ports[i], i);
      pretendMBB.setValidSignature(false);
      pretendMBB.start();
      pretendMBBs.add(pretendMBB);
    }

    fixture.run();
    // Shutdown the MBB.
    for (PretendMBBThread pretendMBB : pretendMBBs) {
      pretendMBB.shutdown();
      pretendMBB.join();
    }

    assertEquals(TestParameters.KEYS_TO_PROCESS, this.progressValues.size());

    float progressIncrement = 100f / TestParameters.KEYS_TO_PROCESS;
    float expectedProgress = 0;

    for (int progress : this.progressValues) {
      expectedProgress += progressIncrement;
      assertEquals(expectedProgress, progress, 0);
    }

    // Test removing a progress listener.
    fixture.removeProgressListener(listener);
  }

  /**
   * Tests the content of the files for each device.
   * 
   * @param serialFile
   *          The generated serial number file.
   * @param randomFile
   *          The generated randomness file.
   * @param commitFile
   *          The generated commit file.
   * @throws IOException
   * @throws JSONException
   */
  private void testFileContent(File serialFile, File randomFile, File commitFile) throws IOException, JSONException {
    // Test that each file has the correct number of content. There should be one row per serial number.
    int serialCount = 0;
    int randomCount = 0;
    int commitCount = 0;

    BufferedReader serialReader = null;
    BufferedReader randomReader = null;
    BufferedReader commitReader = null;

    try {
      serialReader = new BufferedReader(new FileReader(serialFile));
      randomReader = new BufferedReader(new FileReader(randomFile));
      commitReader = new BufferedReader(new FileReader(commitFile));

      String serialLine = serialReader.readLine();
      String randomLine = randomReader.readLine();
      String commitLine = commitReader.readLine();

      while ((serialLine != null) && (randomLine != null) && (commitLine != null)) {
        serialCount++;
        randomCount++;
        commitCount++;

        // Convert the lines into JSON objects.
        JSONObject serialObject = new JSONObject(serialLine);
        JSONObject randomObject = new JSONObject(randomLine);
        JSONObject commitObject = new JSONObject(commitLine);

        // Check the content.
        assertTrue(randomObject.has(MessageFields.RandomnessFile.SERIAL_NO));

        assertTrue(randomObject.has(MessageFields.RandomnessFile.SERIAL_NO));
        assertTrue(randomObject.has(MessageFields.RandomnessFile.RANDOMNESS));

        assertTrue(commitObject.has(MessageFields.RandomnessFile.SERIAL_NO));
        assertTrue(commitObject.has(MessageFields.RandomnessFile.RANDOMNESS));

        assertEquals(serialObject.get(MessageFields.RandomnessFile.SERIAL_NO),
            randomObject.get(MessageFields.RandomnessFile.SERIAL_NO));
        assertEquals(serialObject.get(MessageFields.RandomnessFile.SERIAL_NO),
            commitObject.get(MessageFields.RandomnessFile.SERIAL_NO));

        JSONArray randomArray = (JSONArray) randomObject.get(MessageFields.RandomnessFile.RANDOMNESS);
        assertEquals(TestParameters.COLUMNS, randomArray.length());

        JSONArray commitArray = (JSONArray) commitObject.get(MessageFields.RandomnessFile.RANDOMNESS);
        assertEquals(TestParameters.COLUMNS, commitArray.length());

        // Note that the detailed content of the files is tested in RandomnessWorkerTest.

        serialLine = serialReader.readLine();
        randomLine = randomReader.readLine();
        commitLine = commitReader.readLine();
      }
    }
    // Pass all exceptions up.
    finally {
      if (serialReader != null) {
        serialReader.close();
      }
      if (randomReader != null) {
        randomReader.close();
      }
      if (commitReader != null) {
        commitReader.close();
      }
    }

    // Check the number of objects.
    assertEquals(TestParameters.ROWS, serialCount);
    assertEquals(TestParameters.ROWS, randomCount);
    assertEquals(TestParameters.ROWS, commitCount);
  }

  /**
   * Run the ProcessKeyFiles(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testProcessKeyFiles_1() throws Exception {
    ProcessKeyFiles result = new ProcessKeyFiles(TestParameters.KEYS_TO_PROCESS_FILE_FOLDER, new ConfigFile(
        TestParameters.CONFIG_FILE));
    assertNotNull(result);
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_1() throws Exception {
    ProcessKeyFiles fixture = new ProcessKeyFiles(TestParameters.KEYS_TO_PROCESS_FILE_FOLDER, new ConfigFile(
        TestParameters.CONFIG_FILE));

    fixture.setInnerProgressMonitor(new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        ProcessKeyFilesTest.this.progress = value;
      }
    });

    // Delete all of the old files.
    File outputFolder = new File(TestParameters.OUTPUT_FOLDER);
    TestParameters.deleteRecursive(outputFolder);
    outputFolder.mkdirs();

    // Run some pretend MBBs to receive the data.
    int[] ports = new int[] { 9091, 9092, 9093 };
    List<PretendMBBThread> pretendMBBs = new ArrayList<PretendMBBThread>();

    for (int i = 0; i < ports.length; i++) {
      PretendMBBThread pretendMBB = new PretendMBBThread(ports[i], i);
      pretendMBB.setValidSignature(false);
      pretendMBB.start();
      pretendMBBs.add(pretendMBB);
    }

    // Test running the process with the test files with invalid response signatures.
    fixture.run();

    // Shutdown the MBB.
    for (PretendMBBThread pretendMBB : pretendMBBs) {
      pretendMBB.shutdown();
      pretendMBB.join();
    }

    // Test that we have the required failures.
    List<File> failures = fixture.getFailures();
    assertNotNull(failures);
    assertEquals(TestParameters.KEYS_TO_PROCESS, failures.size());
  }

  /**
   * Run the void run() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testRun_2() throws Exception {
    ProcessKeyFiles fixture = new ProcessKeyFiles(TestParameters.KEYS_TO_PROCESS_FILE_FOLDER, new ConfigFile(
        TestParameters.CONFIG_FILE));

    fixture.setInnerProgressMonitor(new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        ProcessKeyFilesTest.this.progress = value;
      }
    });

    // Delete all of the old files.
    File outputFolder = new File(TestParameters.OUTPUT_FOLDER);
    TestParameters.deleteRecursive(outputFolder);
    outputFolder.mkdirs();

    // Run some pretend MBBs to receive the data.
    int[] ports = new int[] { 9091, 9092, 9093 };
    List<PretendMBBThread> pretendMBBs = new ArrayList<PretendMBBThread>();

    for (int i = 0; i < ports.length; i++) {
      PretendMBBThread pretendMBB = new PretendMBBThread(ports[i], i);
      pretendMBB.start();
      pretendMBBs.add(pretendMBB);
    }

    // Test running the process with the test files.
    fixture.run();

    // Test that we have no failures.
    List<File> failures = fixture.getFailures();
    assertNotNull(failures);
    assertEquals(0, failures.size());

    // Get the keys that should have been processed.
    File keyFilesFolder = new File(TestParameters.PROCESSED_KEYS_FILE_FOLDER);
    File[] keys = keyFilesFolder.listFiles(new PublicKeyFilter());

    // Shutdown and check the MBB received messages and data.
    for (PretendMBBThread pretendMBB : pretendMBBs) {
      pretendMBB.shutdown();
      pretendMBB.join();

      // Test that the correct messages were sent. Only some content is tested.
      List<JSONObject> messages = pretendMBB.getMessages();
      assertEquals(keys.length, messages.size());

      for (JSONObject message : messages) {
        assertTrue(message.has(JSONWBBMessage.TYPE));
        assertTrue(message.has(FileMessage.SENDER_ID));
        assertTrue(message.has(FileMessage.ID));
        assertTrue(message.has(FileMessage.SENDER_SIG));
        assertTrue(message.has(FileMessage.DIGEST));
        assertTrue(message.has(FileMessage.FILESIZE));
        assertTrue(message.has(MessageFields.MixRandomCommit.PRINTER_ID));

        assertEquals(MessageTypes.MIX_RANDOM_COMMIT, message.get(JSONWBBMessage.TYPE));
        assertEquals(TestParameters.SOURCE_SERVER, message.get(FileMessage.SENDER_ID));
      }

      // And that the data was also sent. Content is not tested.
      List<byte[]> data = pretendMBB.getData();
      assertEquals(keys.length, data.size());
    }

    // We should have outputs for each key file.
    for (File key : keys) {
      String deviceID = key.getName().substring(0, key.getName().indexOf("_"));
      File deviceFolder = new File(TestParameters.OUTPUT_FOLDER + "/" + deviceID + "/" + TestParameters.PEER_ID);

      File aesFile = new File(deviceFolder, TestParameters.DEVICE_AES_FILE);
      File commitFile = new File(deviceFolder, TestParameters.DEVICE_COMMIT_DATA_FILE);
      File confirmationFile = new File(deviceFolder, TestParameters.DEVICE_CONFIRMATION_FILE);
      File randomFile = new File(deviceFolder, TestParameters.DEVICE_RANDOM_DATA_FILE);
      File serialFile = new File(deviceFolder, TestParameters.DEVICE_SERIAL_DATA_FILE);
      File wbbFile = new File(deviceFolder, TestParameters.DEVICE_WBB_FILE);

      // Make sure we have the correct set of files.
      File[] deviceFiles = deviceFolder.listFiles();
      Arrays.sort(deviceFiles);

      assertEquals(6, deviceFiles.length);
      assertEquals(aesFile, deviceFiles[0]);
      assertEquals(commitFile, deviceFiles[1]);
      assertEquals(confirmationFile, deviceFiles[2]);
      assertEquals(randomFile, deviceFiles[3]);
      assertEquals(serialFile, deviceFiles[4]);
      assertEquals(wbbFile, deviceFiles[5]);

      // Check that the AES key file contains the correct fields.
      JSONObject aesObject = IOUtils.readJSONObjectFromFile(aesFile.getAbsolutePath());
      assertTrue(aesObject.has(ClientConstants.AESKeyFile.AES_KEY));

      // Check the confirmation file.
      JSONObject confirmationObject = IOUtils.readJSONObjectFromFile(confirmationFile.getAbsolutePath());

      assertEquals(TestParameters.SOURCE_SERVER, confirmationObject.get(MessageFields.RandomnessFile.PEER_ID));
      assertTrue(confirmationObject.has(ConfigFiles.RandomnessConfig.WBBMSG));
      assertEquals(TestParameters.DEVICE_AES_FILE, confirmationObject.get(ConfigFiles.RandomnessConfig.AES_KEY_FILE));
      assertEquals(TestParameters.DEVICE_RANDOM_DATA_FILE,
          confirmationObject.get(ConfigFiles.RandomnessConfig.RANDOMNESS_DATA_FILE));
      assertEquals(TestParameters.DEVICE_COMMIT_DATA_FILE, confirmationObject.get(ConfigFiles.RandomnessConfig.COMMIT_DATA_FILE));

      // Check that the generated data files have the correct content.
      this.testFileContent(serialFile, randomFile, commitFile);

      // Unzip the WBB file and test that it is the same as the commit file.
      File zipFolder = new File(deviceFolder, "zip");
      TestParameters.extractZip(wbbFile, zipFolder);
      File[] zipFiles = zipFolder.listFiles();
      Arrays.sort(zipFiles);

      File zipCommitFile = new File(zipFolder, TestParameters.DEVICE_COMMIT_DATA_FILE);

      assertEquals(1, zipFiles.length);
      assertEquals(zipCommitFile, zipFiles[0]);

      assertTrue(TestParameters.compareFile(commitFile, zipCommitFile));
    }
  }

  /**
   * Run the void setInnerProgressMonitor(ProgressListener) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetInnerProgressMonitor_1() throws Exception {
    ProcessKeyFiles fixture = new ProcessKeyFiles(TestParameters.KEYS_TO_PROCESS_FILE_FOLDER, new ConfigFile(
        TestParameters.CONFIG_FILE));

    // Test adding an inner progress listener.
    ProgressListener listener = new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        ProcessKeyFilesTest.this.progress = value;
      }
    };

    fixture.setInnerProgressMonitor(listener);
    // Run some pretend MBBs to receive the data.
    int[] ports = new int[] { 9091, 9092, 9093 };
    List<PretendMBBThread> pretendMBBs = new ArrayList<PretendMBBThread>();

    for (int i = 0; i < ports.length; i++) {
      PretendMBBThread pretendMBB = new PretendMBBThread(ports[i], i);
      pretendMBB.start();
      pretendMBBs.add(pretendMBB);
    }

    // Test that the listener is used with a newly created GenerateRandomness object.
    this.progress = 0;
    fixture.run();
    // Shutdown the MBB.
    for (PretendMBBThread pretendMBB : pretendMBBs) {
      pretendMBB.shutdown();
      pretendMBB.join();
    }
    assertEquals(100, this.progress);
  }

  /**
   * Run the void setInnerProgressMonitor(ProgressListener) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testSetInnerProgressMonitor_2() throws Exception {
    ProcessKeyFiles fixture = new ProcessKeyFiles(TestParameters.KEYS_TO_PROCESS_FILE_FOLDER, new ConfigFile(
        TestParameters.CONFIG_FILE));

    // Test that the listener is used with an existing GenerateRandomness object.
    this.progress = 0;
    Thread t = new Thread(fixture);
    t.start();
    // fixture.run();
    assertEquals(0, this.progress);

    // Test adding an inner progress listener.
    ProgressListener listener = new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        ProcessKeyFilesTest.this.progress = value;
      }
    };
    // CJC: I modified this because it wasn't actually testing connecting to a running ProcessKeyFiles since the fixture wasn't
    // being
    // run in a separate thread. It was just testing running something twice, which didn't work because the files had already been
    // processed and therefore there no work to do on the second run. This now starts it in a thread and then connects the listener,
    // which then receives the updates
    fixture.setInnerProgressMonitor(listener);

    // Run some pretend MBBs to receive the data.
    int[] ports = new int[] { 9091, 9092, 9093 };
    List<PretendMBBThread> pretendMBBs = new ArrayList<PretendMBBThread>();

    for (int i = 0; i < ports.length; i++) {
      PretendMBBThread pretendMBB = new PretendMBBThread(ports[i], i);
      pretendMBB.start();
      pretendMBBs.add(pretendMBB);
    }
    t.join();
    // Run it again.
    // this.progress = 0;
    // fixture.run();
    // Shutdown the MBB.
    for (PretendMBBThread pretendMBB : pretendMBBs) {
      pretendMBB.shutdown();
      pretendMBB.join();
    }
    assertEquals(100, this.progress);
  }
}