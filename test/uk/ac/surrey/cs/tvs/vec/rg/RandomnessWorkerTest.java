/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>RandomnessWorkerTest</code> contains tests for the class <code>{@link RandomnessWorker}</code>.
 */
public class RandomnessWorkerTest {

  /** Tracks progress. */
  private float progress = 0f;

  /**
   * Run the List<File> call() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testCall_1() throws Exception {
    GenerateRandomness gr = new GenerateRandomness(TestParameters.getInstance().getPublicKey(), TestParameters.COLUMNS,
        TestParameters.ROWS, TestParameters.BIT_SIZE, TestParameters.OUTPUT_RANDOM_DATA_FILE,
        TestParameters.OUTPUT_COMMIT_DATA_FILE);

    // Add a progress listener which will test the progress value being output from the worker.
    gr.addProgressListener(new ProgressListener() {

      @Override
      public void updateProgress(int value) {
        assertTrue(RandomnessWorkerTest.this.progress < value);
        RandomnessWorkerTest.this.progress = value;
      }
    });

    SecretKey key = TestParameters.getInstance().getSecretKey();
    RandomnessWorker fixture = new RandomnessWorker(key, TestParameters.COLUMNS, TestParameters.ROWS, TestParameters.BYTE_SIZE,
        TestParameters.PROGRESS, gr);

    // Create the cipher used to decrypt the values.
    Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
    cipher.init(Cipher.DECRYPT_MODE, key);

    this.progress = 0;
    List<File> result = fixture.call();
    assertEquals(TestParameters.PROGRESS, this.progress, 0);

    // The result should be a list of two files.
    assertNotNull(result);
    assertEquals(2, result.size());

    // Each is a JSON file with ROWS entries of COLUMNS objects.
    int randomCount = 0;
    int commitCount = 0;

    BufferedReader randomReader = null;
    BufferedReader commitReader = null;

    try {
      randomReader = new BufferedReader(new FileReader(result.get(0)));
      commitReader = new BufferedReader(new FileReader(result.get(1)));

      String randomLine = randomReader.readLine();
      String commitLine = commitReader.readLine();

      while ((randomLine != null) && (commitLine != null)) {
        randomCount++;
        commitCount++;

        // Convert the lines into JSON objects and compare them.
        JSONArray randomArray = new JSONArray(randomLine);
        JSONArray commitArray = new JSONArray(commitLine);

        assertEquals(TestParameters.COLUMNS, randomArray.length());
        assertEquals(TestParameters.COLUMNS, commitArray.length());

        // Test that each random object contains the fields we expect.
        for (int i = 0; i < randomArray.length(); i++) {
          JSONObject randomObject = (JSONObject) randomArray.get(i);

          assertTrue(randomObject.has(MessageFields.RandomnessFile.RANDOM_VALUE));
          assertTrue(randomObject.has(MessageFields.RandomnessFile.RANDOM_VALUE_COMMIT));

          // Decrypt the fields.
          byte[] rEncrypt = IOUtils.decodeData(EncodingType.HEX,
              (String) randomObject.get(MessageFields.RandomnessFile.RANDOM_VALUE));
          byte[] r = cipher.doFinal(rEncrypt);

          byte[] rCommEncrypt = IOUtils.decodeData(EncodingType.HEX,
              (String) randomObject.get(MessageFields.RandomnessFile.RANDOM_VALUE_COMMIT));
          byte[] rComm = cipher.doFinal(rCommEncrypt);

          // Construct what the commit data should be and compare it.
          String commitExpected = IOUtils.encodeData(EncodingType.HEX, CryptoUtils.generateCommitment(r, rComm));

          assertEquals(commitExpected, commitArray.get(i));
        }

        randomLine = randomReader.readLine();
        commitLine = commitReader.readLine();
      }
    }
    // Pass all exceptions up.
    finally {
      if (randomReader != null) {
        randomReader.close();
      }
      if (commitReader != null) {
        commitReader.close();
      }
    }

    // Check the number of objects.
    assertEquals(TestParameters.ROWS, randomCount);
    assertEquals(TestParameters.ROWS, commitCount);

    // Tidy up the files.
    File randomFolder = new File(TestParameters.RANDOM_FOLDER);
    TestParameters.deleteRecursive(randomFolder);
    randomFolder.mkdirs();
  }

  /**
   * Run the RandomnessWorker(SecretKey,int,int,int,float,GenerateRandomness) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRandomnessWorker_1() throws Exception {
    GenerateRandomness gr = new GenerateRandomness(TestParameters.getInstance().getPublicKey(), TestParameters.COLUMNS,
        TestParameters.ROWS, TestParameters.BIT_SIZE, TestParameters.OUTPUT_RANDOM_DATA_FILE,
        TestParameters.OUTPUT_COMMIT_DATA_FILE);

    RandomnessWorker result = new RandomnessWorker(TestParameters.getInstance().getSecretKey(), TestParameters.COLUMNS,
        TestParameters.ROWS, TestParameters.BYTE_SIZE, TestParameters.PROGRESS, gr);

    assertNotNull(result);
  }
}