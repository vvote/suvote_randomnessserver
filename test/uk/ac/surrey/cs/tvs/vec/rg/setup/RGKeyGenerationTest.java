/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - modified from CodePro test generation
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg.setup;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import uk.ac.surrey.cs.tvs.TestParameters;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.KeyStoreConfig;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * The class <code>RGKeyGenerationTest</code> contains tests for the class <code>{@link RGKeyGeneration}</code>.
 */
public class RGKeyGenerationTest {

  /**
   * Perform pre-test initialisation.
   * 
   * @throws Exception
   *           if the initialisation fails for some reason
   */
  @Before
  public void setUp() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Perform post-test clean-up.
   * 
   * @throws Exception
   *           if the clean-up fails for some reason
   */
  @After
  public void tearDown() throws Exception {
    // Tidy up the files.
    TestParameters.tidyFiles();
  }

  /**
   * Run the void generateKeyAndCSR() method test.
   * 
   * @throws Exception
   */
  @Test
  public void testGenerateKeyAndCSR_1() throws Exception {
    // Copy the config file so we can change the key store.
    JSONObject config = IOUtils.readJSONObjectFromFile(TestParameters.CONFIG_FILE);

    File keys = File.createTempFile(TestParameters.CONFIG_FILE_PREFIX, ".bks");
    keys.deleteOnExit();
    config.put(KeyStoreConfig.SIGNATURE_KEY_PATH, keys.getAbsolutePath());
    Files.copy(FileSystems.getDefault().getPath("./testdata/server1/keys.bks"), FileSystems.getDefault().getPath(keys.getAbsolutePath()),
        StandardCopyOption.REPLACE_EXISTING);

    File file = File.createTempFile(TestParameters.CONFIG_FILE_PREFIX, TestParameters.CONFIG_FILE_SUFFIX);
    file.deleteOnExit();

    IOUtils.writeJSONToFile(config, file.getAbsolutePath());

    RGKeyGeneration fixture = new RGKeyGeneration(file.getAbsolutePath());

    fixture.generateKeyAndCSR();

    // Test certificate creation. We don't test the content because this is tested elsewhere.
    //File signingFile = new File(TestParameters.CSR_FOLDER, TestParameters.SOURCE_SERVER + RGKeyGeneration.SIGNING_SUFFIX + ".csr");
    File sslFile = new File(TestParameters.CSR_FOLDER, TestParameters.SOURCE_SERVER + RGKeyGeneration.SSL_SUFFIX + ".csr");

    //assertTrue(signingFile.exists());
    assertTrue(sslFile.exists());
  }

  /**
   * Run the void importCert(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testImportCert_1() throws Exception {
    // Copy the config file so we can change the key store.
    JSONObject config = IOUtils.readJSONObjectFromFile(TestParameters.CONFIG_FILE);

    File keys = File.createTempFile(TestParameters.CONFIG_FILE_PREFIX, ".bks");
    keys.deleteOnExit();
    config.put(KeyStoreConfig.SIGNATURE_KEY_PATH, keys.getAbsolutePath());
    Files.copy(FileSystems.getDefault().getPath("./testdata/server1/keys.bks"), FileSystems.getDefault().getPath(keys.getAbsolutePath()),
        StandardCopyOption.REPLACE_EXISTING);

    File file = File.createTempFile(TestParameters.CONFIG_FILE_PREFIX, TestParameters.CONFIG_FILE_SUFFIX);
    file.deleteOnExit();

    IOUtils.writeJSONToFile(config, file.getAbsolutePath());

    RGKeyGeneration fixture = new RGKeyGeneration(file.getAbsolutePath());

    // Test certificate import.

    File sslFile = new File(TestParameters.IMPORT_FOLDER, TestParameters.SOURCE_SERVER + RGKeyGeneration.SSL_SUFFIX + ".pem");

    fixture.importCert(sslFile.getAbsolutePath());
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_1() throws Exception {
    String[] args = new String[] {};

    RGKeyGeneration.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_2() throws Exception {
    // Copy the config file so we can change the key store.
    JSONObject config = IOUtils.readJSONObjectFromFile(TestParameters.CONFIG_FILE);

    File keys = File.createTempFile(TestParameters.CONFIG_FILE_PREFIX, ".bks");
    keys.deleteOnExit();
    config.put(KeyStoreConfig.SIGNATURE_KEY_PATH, keys.getAbsolutePath());
    Files.copy(FileSystems.getDefault().getPath("./testdata/server1/keys.bks"), FileSystems.getDefault().getPath(keys.getAbsolutePath()),
        StandardCopyOption.REPLACE_EXISTING);

    File file = File.createTempFile(TestParameters.CONFIG_FILE_PREFIX, TestParameters.CONFIG_FILE_SUFFIX);
    file.deleteOnExit();

    IOUtils.writeJSONToFile(config, file.getAbsolutePath());

    String[] args = new String[] { file.getAbsolutePath(), "gen" };

    RGKeyGeneration.main(args);
  }

  /**
   * Run the void main(String[]) method test.
   * 
   * @throws Exception
   */
  @Test
  public void testMain_3() throws Exception {
    // Copy the config file so we can change the key store.
    JSONObject config = IOUtils.readJSONObjectFromFile(TestParameters.CONFIG_FILE);

    File keys = File.createTempFile(TestParameters.CONFIG_FILE_PREFIX, ".bks");
    keys.deleteOnExit();
    config.put(KeyStoreConfig.SIGNATURE_KEY_PATH, keys.getAbsolutePath());
    Files.copy(FileSystems.getDefault().getPath("./testdata/server1/keys.bks"), FileSystems.getDefault().getPath(keys.getAbsolutePath()),
        StandardCopyOption.REPLACE_EXISTING);

    File file = File.createTempFile(TestParameters.CONFIG_FILE_PREFIX, TestParameters.CONFIG_FILE_SUFFIX);
    file.deleteOnExit();

    IOUtils.writeJSONToFile(config, file.getAbsolutePath());

    RGKeyGeneration fixture = new RGKeyGeneration(file.getAbsolutePath());

    // Create the certificates.
    fixture.generateKeyAndCSR();

    // Test import from the command line.
    String[] args = new String[] { file.getAbsolutePath(), "import",
        TestParameters.CSR_FOLDER + File.separator + TestParameters.SOURCE_SERVER + RGKeyGeneration.SSL_SUFFIX + ".csr" };

    RGKeyGeneration.main(args);
  }

  /**
   * Run the RGKeyGeneration(String) constructor test.
   * 
   * @throws Exception
   */
  @Test
  public void testRGKeyGeneration_1() throws Exception {
    RGKeyGeneration result = new RGKeyGeneration(TestParameters.CONFIG_FILE);
    assertNotNull(result);
  }
}