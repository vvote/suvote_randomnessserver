/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Matthew Casey - initial version
 ******************************************************************************/
package uk.ac.surrey.cs.tvs;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.json.JSONException;

import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.vec.rg.SerialNumberGenerator;

/**
 * This class defines common test parameters and static initialisers within a singleton.
 */
public class TestParameters {

  /** Number of columns in randomness. */
  public static final int       COLUMNS                     = 80;

  /** Number of rows in randomness. */
  public static final int       ROWS                        = 1000;

  /** Randomness bit size. */
  public static final int       BIT_SIZE                    = 256;

  /** Randomness byte size. */
  public static final int       BYTE_SIZE                   = BIT_SIZE / 8;

  /** Progress proportion. */
  public static final float     PROGRESS                    = 100f;

  /** Input public key file. */
  public static final String    PUBLIC_KEY_FILE             = "PublicKey.json";

  /** Output file for the AES key for a device. */
  public static String          DEVICE_AES_FILE             = "AESKey.json";

  /** Output file for the randomness data file. */
  public static String          DEVICE_RANDOM_DATA_FILE     = "randomData.json";

  /** Output file for the commit data file. */
  public static String          DEVICE_COMMIT_DATA_FILE     = "commitData.json";

  /** Output file for the serial number data file. */
  public static String          DEVICE_SERIAL_DATA_FILE     = "serials.json";

  /** Output file for the WBB submission file. */
  public static String          DEVICE_WBB_FILE             = "wbbsubmission.zip";

  /** Output file for the MBB confirmation. */
  public static String          DEVICE_CONFIRMATION_FILE    = "conf.json";

  /** Output folder for files. */
  public static final String    OUTPUT_FOLDER               = "./testdata/server1/output";

  /** Output folder for random files. */
  public static final String    RANDOM_FOLDER               = "./testdata/server1/randDir";

  /** Output folder for certificates. */
  public static final String    CSR_FOLDER                  = "./testdata/server1/csrs";

  /** Import folder for certificates. */
  public static final String    IMPORT_FOLDER               = "./testdata/server1/import";

  /** Output file for randomness data. */
  public static final String    OUTPUT_RANDOM_DATA_FILE     = OUTPUT_FOLDER + "/" + DEVICE_RANDOM_DATA_FILE;

  /** Output file for commit data. */
  public static final String    OUTPUT_COMMIT_DATA_FILE     = OUTPUT_FOLDER + "/" + DEVICE_COMMIT_DATA_FILE;

  /** Output file for serial number data. */
  public static final String    OUTPUT_SERIAL_DATA_FILE     = OUTPUT_FOLDER + "/" + DEVICE_SERIAL_DATA_FILE;

  /** Key file folder for processing matching to server_conf.json. */
  public static final String    KEYS_TO_PROCESS_FILE_FOLDER = "./testdata/server1/keys_to_process";

  /** Key file folder processed matching to server_conf.json. */
  public static final String    PROCESSED_KEYS_FILE_FOLDER  = "./testdata/server1/processed_keys";

  /** Number of keys to process corresponding to the files in the KEY_FILE_FOLDER. */
  public static final int       KEYS_TO_PROCESS             = 1;

  /** The peer id matching the server_conf.json. */
  public static final String    PEER_ID                     = "MixServer1";

  /** Test prefix. */
  public static final String    PREFIX                      = "testDevice";

  /** Dummy name used for testing. */
  public static final String    NAME                        = "myName";

  /** Public/private key size. */
  public static final int       PAIR_KEY_SIZE               = 272;

  /** Secret key size. */
  public static final int       SECRET_KEY_SIZE             = 256;

  /** The randomness server configuration file prefix. */
  public static final String    CONFIG_FILE_PREFIX          = "server_conf";
  public static final String    CONFIG_FILE_PATH            = "./testdata/server1/";
  /** The randomness server configuration file suffix. */
  public static final String    CONFIG_FILE_SUFFIX          = ".json";

  /** The randomness server configuration file. */
  public static final String    CONFIG_FILE                 = CONFIG_FILE_PATH + CONFIG_FILE_PREFIX + CONFIG_FILE_SUFFIX;

  /** The cipher suites used for SSL connections. */
  public static final String[]  CIPHERS                     = new String[] { "SSL_RSA_WITH_NULL_SHA" };

  /** Commit time for messages. */
  public static final String    COMMIT_TIME                 = "18:00";

  /** The signing key store. */
  public static final String    KEY_STORE                   = "./testdata/server1/keys.bks";
  public static final String    MBB_KEY_STORE               = "./testdata/mbb/Peer1keys.jks";
  public static final String    MBB_SIGNKEY_STORE           = "./testdata/mbb/Peer1_private.bks";
  public static final String    MBB_SIGNKEY_STORE_BASE      = "./testdata/mbb/";
  public static final String    MBB_SIGNKEY_STORE_SUFFIX    = "_private.bks";

  /** The SSL key store password. */
  public static final String    KEY_STORE_PASSWORD          = "";

  /** Peer used for signing. */
  public static final String[]  SIGNING_PEERS               = { "Peer1", "Peer2", "Peer3" };

  /** Source server. */
  public static final String    SOURCE_SERVER               = "MixServer1";

  /** Test device. */
  public static final String    DEVICE                      = "TestDeviceOne";

  /** Default buffer size to use when copying files. */
  private static final int      BUFFER_SIZE                 = 8192;

  /** The singleton instance of these parameters. */
  private static TestParameters instance                    = null;

  /** Public key used for testing. */
  private PublicKey             publicKey                   = null;

  /** Secret key used for testing. */
  private SecretKey             secretKey                   = null;

  /**
   * Private default constructor to prevent instantiation.
   */
  private TestParameters() {
    super();

    // Initialise BouncyCastle.
    CryptoUtils.initProvider();
  }

  /**
   * Generates a temporary file containing generated device serial numbers. The file is automatically deleted on exit. This method
   * makes use of the {@link SerialNumberGenerator#generateSerialNumbers(int, String, String)} method, and hence this is assumed to
   * be working correctly.
   * 
   * @param rows
   *          The number of serial numbers to generate.
   * @return The new file.
   * @throws IOException
   * @throws JSONException
   */
  public File generateSerialNumberFile(int rows) throws IOException, JSONException {
    File folder = new File(OUTPUT_FOLDER);
    folder.mkdirs();

    File serialFile = File.createTempFile("serial", ".tmp", folder);
    serialFile.deleteOnExit();

    SerialNumberGenerator.generateSerialNumbers(rows, serialFile.getAbsolutePath(), TestParameters.PREFIX);

    return serialFile;
  }

  /**
   * Returns the public key used for testing. The key is generated on first use.
   * 
   * @return The public key.
   * @throws NoSuchAlgorithmException
   * @throws JSONIOException
   * @throws CryptoIOException
   * @throws NoSuchProviderException
   */
  public PublicKey getPublicKey() throws NoSuchAlgorithmException, CryptoIOException, JSONIOException, NoSuchProviderException {
    // Lazy creation.
    if (this.publicKey == null) {
      KeyPairGenerator keyPairGen = KeyPairGenerator.getInstance("ElGamal", "BC");
      SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
      keyPairGen.initialize(PAIR_KEY_SIZE, random);
      KeyPair pair = keyPairGen.generateKeyPair();

      this.publicKey = pair.getPublic();
    }

    return this.publicKey;
  }

  /**
   * Returns the secret key used for testing. The key is generated on first use.
   * 
   * @return The secret key.
   * @throws NoSuchAlgorithmException
   * @throws NoSuchPaddingException
   * @throws NoSuchProviderException
   */
  public SecretKey getSecretKey() throws NoSuchAlgorithmException, NoSuchProviderException, NoSuchPaddingException {
    // Lazy creation.
    if (this.secretKey == null) {
      KeyGenerator keyGen = KeyGenerator.getInstance("AES");
      SecureRandom krandom = new SecureRandom();
      keyGen.init(SECRET_KEY_SIZE, krandom);

      this.secretKey = keyGen.generateKey();
    }

    return this.secretKey;
  }

  /**
   * Compares the content of two files.
   * 
   * @param file1
   *          Input file 1.
   * @param file2
   *          Input file 2.
   * @return True if the files are identical, false otherwise.
   * @throws IOException
   *           on failure to read.
   */
  public static boolean compareFile(File file1, File file2) throws IOException {
    boolean result = true;

    byte[] buffer1 = new byte[BUFFER_SIZE];
    byte[] buffer2 = new byte[BUFFER_SIZE];

    InputStream in1 = null;
    InputStream in2 = null;

    try {
      in1 = new FileInputStream(file1);
      in2 = new FileInputStream(file2);

      int numBytes1 = in1.read(buffer1);
      int numBytes2 = in2.read(buffer2);

      while ((numBytes1 != -1) && (numBytes2 != -1) && result) {
        if (numBytes1 != numBytes2) {
          result = false;
        }
        else {
          for (int i = 0; i < numBytes1; i++) {
            if (buffer1[i] != buffer2[i]) {
              result = false;
            }
          }
        }

        numBytes1 = in1.read(buffer1);
        numBytes2 = in2.read(buffer2);
      }
    }
    // Throw exceptions up.
    finally {
      if (in1 != null) {
        in1.close();
      }
      if (in2 != null) {
        in2.close();
      }
    }

    return result;
  }

  /**
   * Copies an input stream to an output stream. Both streams are left at the end of their read/write.
   * 
   * @param in
   *          The destination stream.
   * @param out
   *          The source stream.
   * 
   * @throws IOException
   *           on failure to read or write.
   */
  public static void copyStream(InputStream in, OutputStream out) throws IOException {
    byte[] buffer = new byte[BUFFER_SIZE];

    int numBytes = in.read(buffer);

    while (numBytes != -1) {
      out.write(buffer, 0, numBytes);
      numBytes = in.read(buffer);
    }
  }

  /**
   * Recursive method to delete a folder and its content. If something goes wrong, this method remains silent.
   * 
   * @param file
   *          The file/folder to delete.
   */
  public static void deleteRecursive(File file) {
    if (file.isDirectory()) {
      for (File child : file.listFiles()) {
        deleteRecursive(child);
      }
    }

    file.delete();
  }

  /**
   * Extracts a zip file into the specified directory.
   * 
   * @param source
   *          The ZIP file.
   * @param target
   *          The target directory.
   * @return True if the extraction was successful, false otherwise.
   * @throws IOException
   */
  public static boolean extractZip(File source, File target) throws IOException {
    // Open the ZIP archive.
    ZipInputStream zipInputStream = null;
    OutputStream outputStream = null;
    boolean success = false;

    try {
      zipInputStream = new ZipInputStream(new BufferedInputStream(new FileInputStream(source)));

      ZipEntry zipEntry = zipInputStream.getNextEntry();

      while (zipEntry != null) {
        // Extract the file name, including any sub-directory names. Sub-directories are created.
        File outputFile = new File(target, zipEntry.getName());
        outputFile.getParentFile().mkdirs();

        // Extract the file.
        outputStream = new FileOutputStream(outputFile);
        copyStream(zipInputStream, outputStream);
        outputStream.close();
        outputStream = null;

        // Get the next entry.
        zipEntry = zipInputStream.getNextEntry();
      }

      success = true;
    }
    // Throw exceptions up.
    finally {
      // Close any open output stream.
      if (outputStream != null) {
        outputStream.close();
      }

      // Close the input stream.
      if (zipInputStream != null) {
        zipInputStream.close();
      }
    }

    return success;
  }

  /**
   * Singleton access method.
   * 
   * @return The test parameters singleton instance.
   */
  public static TestParameters getInstance() {
    // Lazy creation.
    if (instance == null) {
      instance = new TestParameters();
    }

    return instance;
  }

  /**
   * Signs the specified content using the signing key.
   * 
   * @param data
   *          The data to sign.
   * @return The signature.
   * @throws TVSSignatureException
   * @throws CryptoIOException
   */
  public static String signData(String data, int peer) throws TVSSignatureException, CryptoIOException {

    try {
      TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKeyStore.load(MBB_SIGNKEY_STORE_BASE + SIGNING_PEERS[peer] + MBB_SIGNKEY_STORE_SUFFIX, KEY_STORE_PASSWORD.toCharArray());
      TVSSignature signature = new TVSSignature(SignatureType.BLS, tvsKeyStore.getBLSPrivateKey(SIGNING_PEERS[peer] + "_SigningSK2"));
      signature.update(data);
      return signature.signAndEncode(EncodingType.BASE64);
    }
    catch (KeyStoreException e) {
      throw new CryptoIOException(e);
    }

  }

  /**
   * Deletes all of the test output files that are generated.
   */
  public static void tidyFiles() {
    deleteRecursive(new File(OUTPUT_FOLDER));
    deleteRecursive(new File(RANDOM_FOLDER));
    deleteRecursive(new File(CSR_FOLDER));
  }
}