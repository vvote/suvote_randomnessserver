Randomness Server UI
====================

Project for generating randomenss on independent servers for vVote. Commits to randomness on the MBB, before preparing files to be sent to the Client Device.

Overview
========

Each client should generated an ElGamal public key pair. The public keys of all the clients are collected and placed in the "keys\_to\_process" folder (or whatever folder is specified by the keyToProcess value in the config file). When run the RandomnessServer will iterate through each key. For each key it will generate enough randomness for a ballot with a the number of columns (candidates across all races) and rows (ballots) specified in the config file. The amount of randomness being generated could be substantial and runtime is therefore dependent on the availability of randomness. There is a simple web-based UI that is available via localhost (port specified in config file).

Running
=======

The randomness server can be started by calling:

RandomnessServerUI starts the web server that will serve the user interface for the RandomnessServer. A configuration file is required. If no configuration file is specified the default path at ./server_conf.json will be checked.

Usage:
    RandomnessServerUI pathToConfigFile
    pathToConfigFile: optional parameter to explicitly state path to configuration to use.


Care should be taken when running this in a virtual environment. It is dependent on the quality and availability of good quality randomness from the underlying OS. Virtualised systems typically do not provide good randomness and should be avoided. They also tend to produce randomness at a slower rate, that could lead to blocking and slow performance of the randomness generation.
