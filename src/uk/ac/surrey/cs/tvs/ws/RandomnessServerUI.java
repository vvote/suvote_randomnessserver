/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.ws;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.java_websocket.WebSocket;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.comms.http.NanoServlet;
import uk.ac.surrey.cs.tvs.comms.http.VECWebSocket;
import uk.ac.surrey.cs.tvs.comms.http.VECWebSocketException;
import uk.ac.surrey.cs.tvs.comms.http.VECWebSocketListener;
import uk.ac.surrey.cs.tvs.comms.http.VVoteWebServer;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.ui.ProgressMonitor;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.JSONUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.vec.rg.ProcessKeyFiles;

/**
 * Creates a local web server and web socket to run HTML UI from. Handles messages from websockets and updating progress messages.
 * 
 * @author Chris Culnane
 * 
 */
public class RandomnessServerUI implements VECWebSocketListener {

  /**
   * Default location and filename of the server config - can be overridden at the start
   */
  private static final String DEFAULT_CONFIG_PATH     = "./server_conf.json";

  /**
   * The address to listen on - always localhost should not be accessible without machine level access (tunnel)
   */
  private static final String LISTEN_ADDRESS          = "localhost";

  /**
   * Location of JSON file that stores the dynamically assigned web socket port - this file is read by front-end to connect to
   * dynamic web socket
   */
  private static final String LOCALPORT_FILE          = "js/localport.json";

  /**
   * Logger
   */
  private static final Logger logger                  = LoggerFactory.getLogger(RandomnessServerUI.class);

  /**
   * Name for thread that processes the keys
   */
  private static final String PROCESS_KEY_THREAD_NAME = "KeyProcessorThread";

  /**
   * Identifier for update to the fileProgress bar - covers the progress of the processing of an individual key file
   */
  private static final String UI_FILE_PROGRESS        = "fileProgress";

  /**
   * Identifier for update to filesProgress bar - this covers the processing of set of key files
   */
  private static final String UI_FILES_PROGRESS       = "filesProgress";

  /**
   * Field in WebSocket JSON message
   */
  private static final String UI_TYPE                 = "type";

  /**
   * Value of type field in JSON message for generating randomness
   */
  private static final String UI_TYPE_GEN_RANDOMNESS  = "genRandomness";

  /**
   * Value of type field in JSON message for getting a list of error files
   */
  private static final String UI_TYPE_GET_FAILURES    = "getFailures";

  /**
   * Value of type field in JSON response message to indicate an error
   */
  private static final String UI_TYPE_ERROR           = "error";

  /**
   * Value of message field for JSON response
   */
  private static final String UI_TYPE_MSG             = "msg";

  /**
   * Holds a reference to the ConfigFile for the randomness server
   */
  private ConfigFile          conf;

  /**
   * ProcessKeyFiles object that keeps a reference to the current ProcessKeyFiles object
   */
  private ProcessKeyFiles     currentPkf;

  /**
   * Thread object that keeps a reference to the current ProcessKeyFiles thread
   */
  private Thread              currentPkfThread;

  /**
   * SimpleWebServer for serving the front-end UI content
   */
  private VVoteWebServer      sws;

  /**
   * WebSocket for providing front-end actions and progress updates
   */
  private VECWebSocket        vws;

  /**
   * Schema for the config file.
   */
  private static final String CONFIG_SCHEMA           = "./schemas/configschema.json";
  /**
   * Schema for UI messages
   */
  private static final String UI_SCHEMA           = "./schemas/uischema.json";
  
  /**
   * Variable to hold the UISchema
   */
  private String uiSchema;
  /**
   * List of Files that have failed during uploads
   */
  private List<File>             failures          = new ArrayList<File>();

  /**
   * Creates new VECWebSocket that listens on the specified address
   * 
   * @param configFile
   *          String path to configuration file
   * @throws JSONIOException
   * @throws IOException
   * @throws JSONException
   * @throws VECWebSocketException
   */
  public RandomnessServerUI(String configFile) throws JSONIOException, IOException, VECWebSocketException, JSONException {
    super();

    this.conf = new ConfigFile(configFile, CONFIG_SCHEMA);
    CryptoUtils.initProvider();
    this.sws = new VVoteWebServer(RandomnessServerUI.LISTEN_ADDRESS,
        this.conf.getIntParameter(ConfigFiles.RandomnessServerConfig.UI_PORT), new File(
            this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.UI_FOLDER)), new HashMap<String, NanoServlet>());
    this.sws.start();

    InetSocketAddress listenAddress = new InetSocketAddress(RandomnessServerUI.LISTEN_ADDRESS, 0);
    this.vws = new VECWebSocket(listenAddress);
    this.vws.addWebSocketListener(this);
    this.vws.start();
    this.vws.waitForStartAndUpdatePort(new File(this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.UI_FOLDER),
        LOCALPORT_FILE));
    this.uiSchema=JSONUtils.loadSchema(UI_SCHEMA);
    logger.info("WebSocket created on {}", listenAddress.getAddress().getHostAddress());
  }

  /**
   * Processes an incoming message and acts upon it
   * 
   * 
   * @param msg
   *          JSONObject containing the message
   * @param ws
   *          WebSocket we are using
   * @throws JSONException
   * @throws JSONIOException
   */
  @Override
  public void processMessage(JSONObject msg, WebSocket ws) {
    // Check message has type field
    try {
      if(!JSONUtils.validateSchema(this.uiSchema, msg.toString())){
        logger.warn("Incoming message failed JSONSchema validation: {}", msg.toString());
        JSONObject response = new JSONObject();
        response.put(RandomnessServerUI.UI_TYPE, RandomnessServerUI.UI_TYPE_ERROR);
        response.put(RandomnessServerUI.UI_TYPE_MSG, "Unknown message");
        ws.send(response.toString());
        return;
      }
      if (msg.has(RandomnessServerUI.UI_TYPE)
          && msg.getString(RandomnessServerUI.UI_TYPE).equalsIgnoreCase(RandomnessServerUI.UI_TYPE_GEN_RANDOMNESS)) {
        // Generate Randomness
        // Create a ProgressMonitor for the files progress (i.e. progress through all files) and one for progress within an
        // individual file
        ProgressMonitor pm = new ProgressMonitor(RandomnessServerUI.UI_FILES_PROGRESS, ws);
        ProgressMonitor pmFile = new ProgressMonitor(RandomnessServerUI.UI_FILE_PROGRESS, ws);

        // Check if we have an existing thread that is alive - i.e. the front-end is reconnecting to an already running process,
        // we want to reconnect to the progress monitor
        if (this.currentPkfThread != null && this.currentPkfThread.isAlive()) {
          logger.info("Reconnecting to existing PublicKeyFiles thread");
          this.currentPkf.setInnerProgressMonitor(pmFile);
          this.currentPkf.addProgressListener(pm);
        }
        else {
          logger.info("Creating new PublicKeyFiles thread");
          this.currentPkf = new ProcessKeyFiles(this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.KEYS_TO_PROCESS),
              this.conf);
          this.currentPkf.setFailuresList(this.failures);
          this.currentPkf.setInnerProgressMonitor(pmFile);
          this.currentPkf.addProgressListener(pm);
          this.currentPkfThread = new Thread(this.currentPkf, RandomnessServerUI.PROCESS_KEY_THREAD_NAME);
          this.currentPkfThread.start();
        }
      }
      else if (msg.has(RandomnessServerUI.UI_TYPE)
          && msg.getString(RandomnessServerUI.UI_TYPE).equalsIgnoreCase(RandomnessServerUI.UI_TYPE_GET_FAILURES)) {
        // Check if we have an existing thread that is alive - i.e. the front-end is reconnecting to an already running process,
        // we want to reconnect to the progress monitor
        if (this.currentPkfThread != null && this.currentPkfThread.isAlive()) {
          logger.info("Reconnecting to existing PublicKeyFiles thread");
          JSONArray failures = new JSONArray(this.currentPkf.getFailures());
          JSONObject response = new JSONObject();
          response.put(RandomnessServerUI.UI_TYPE, RandomnessServerUI.UI_TYPE_GET_FAILURES);
          response.put(RandomnessServerUI.UI_TYPE_MSG, failures);
          ws.send(response.toString());
        }
        else {
          JSONArray failures = new JSONArray(this.failures);
          JSONObject response = new JSONObject();
          response.put(RandomnessServerUI.UI_TYPE, RandomnessServerUI.UI_TYPE_GET_FAILURES);
          response.put(RandomnessServerUI.UI_TYPE_MSG, failures);
          ws.send(response.toString());
        }
      }
    }
    catch (JSONException e) {
      logger.error("Exception whilst processing message", e);
    }
    catch (JSONIOException e) {
      logger.error("Exception whilst processing message", e);
    }
    catch (PeerSSLInitException e) {
      logger.error("Exception whilst processing message", e);
    }
  }

  /**
   * Main method that starts the server, which runs on a separate thread.
   * 
   * @param args
   *          Command line arguments.
   */
  public static void main(String[] args) {
    try {
      if (args.length == 0) {
        new RandomnessServerUI(RandomnessServerUI.DEFAULT_CONFIG_PATH);
      }
      else if (args.length == 1) {
        new RandomnessServerUI(args[0]);
      }
      else {
        RandomnessServerUI.printUsage();
      }
    }
    catch (JSONException | JSONIOException | IOException | VECWebSocketException e) {
      System.out.println("An exception has occured whilst running the RandomnessServerUI");
      RandomnessServerUI.printUsage();
      logger.error("Exception whilst starting RandomnessServerIO", e);
    }
  }

  /**
   * Prints the usage instructions for starting the UI
   */
  private static final void printUsage() {
    System.out.println("Usage of RandomnessServerUI:");
    System.out.println("RandomnessServerUI starts the web server that will serve");
    System.out.println("the user interface for the RandomnessServer. A configuration");
    System.out.println("file is required. If no configuration file is specified ");
    System.out.println("the default path at ./server_conf.json will be checked.");
    System.out.println("Usage:\n");
    System.out.println("RandomnessServerUI [pathToConfigFile] \n");
    System.out.println("\tpathToConfigFile: optional parameter to explicitly state path to configuration to use.");
  }
  
  /**
   * Shutsdown the web servers that are listening
   * @throws InterruptedException 
   * @throws IOException 
   */
  public void shutdown() throws IOException, InterruptedException{
    logger.info("Shutting down RandomnessServerUI");
    this.sws.stop();
    this.vws.stop();
  }
}
