/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg.setup;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.KeyStoreConfig;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.KeyGeneration;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CertificateStoringException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.KeyGenerationException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSKeyPair;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;

/**
 * Randomness Generation Key Generation. Used for generating key pairs, CSR and importing completed certificates.
 * 
 * @author Chris Culnane
 * 
 */
public class RGKeyGeneration extends KeyGeneration {

  /**
   * Static string to specify the generate option
   */
  private static final String GENERATE_OPTION = "gen";

  /**
   * Static string to specify the import option
   */
  private static final String IMPORT_OPTION   = "import";

  /**
   * Logger
   */
  private static final Logger logger          = LoggerFactory.getLogger(RGKeyGeneration.class);

  /**
   * Constant SigningKey Suffix used to distinguish signing keys for the RandomnessServer
   */
  public static final String  SIGNING_SUFFIX  = "";

  /**
   * Constant SSLSuffix to distinguish the SSL key from the signing key
   */
  public static final String  SSL_SUFFIX      = "_SSL";

  /**
   * ConfigFile for the RandomnessServer
   */
  private ConfigFile          randConf;

  /**
   * Constructs a new RGKeyGeneration class that can be used to generate keys, CSRs and import certificates
   * 
   * @param configFile
   *          String path to the RandomnessServer config file
   * 
   * @throws JSONIOException
   * 
   */
  public RGKeyGeneration(String configFile) throws JSONIOException {
    super("");

    this.randConf = new ConfigFile(configFile);
    this.id = this.randConf.getStringParameter(ConfigFiles.RandomnessServerConfig.PEER_ID);
  }

  /**
   * Generates the two keys the RandomnessServer requires and the corresponding CSRs. The first key is a signing key, the second is
   * an SSL key
   * 
   * @throws KeyGenerationException
   * @throws TVSSignatureException
   */
  public void generateKeyAndCSR() throws KeyGenerationException, TVSSignatureException {
    try {
      TVSKeyStore ks = TVSKeyStore.getInstance(KeyStoreConfig.KEY_STORE_TYPE);
      ks.initKeyStore();
      // ks.load(this.randConf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PATH),this.randConf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PWD).toCharArray());

      File csrOutput = new File(this.randConf.getStringParameter(ConfigFiles.RandomnessServerConfig.CSR_FOLDER));
      IOUtils.checkAndMakeDirs(csrOutput);

      BLSKeyPair bkp = BLSKeyPair.generateKeyPair();
      ks.addBLSKeyPair(bkp, this.id + SIGNING_SUFFIX);
      this.generateKey(SSL_SUFFIX, new File(csrOutput, this.id + SSL_SUFFIX + ".csr"), ks.getKeyStore(), SignatureType.RSA);

      // Store the public key part for distribution
      ks.exportPublicKeyAsPublicStore(this.id + SIGNING_SUFFIX,
          (new File(csrOutput, this.id + SIGNING_SUFFIX + ".blCert")).getAbsolutePath());

      // Rename the TVSKeyStore to replace .bks with .jks for the underlying keystore - this only needed during initialisation
      File keyStoreFile = new File(this.randConf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PATH));
      File outputFile = new File(keyStoreFile.getParentFile(), keyStoreFile.getName().replace(".bks", ".jks"));

      ks.store(this.randConf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PATH), outputFile.getAbsolutePath(), this.randConf
          .getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PWD).toCharArray());

    }
    catch (KeyStoreException e) {
      throw new KeyGenerationException("Exception whilst generating keys", e);
    }
    catch (FileNotFoundException e) {
      throw new KeyGenerationException("Exception whilst generating keys", e);
    }
    catch (IOException e) {
      throw new KeyGenerationException("Exception whilst generating keys", e);
    }
    catch (NoSuchAlgorithmException e) {
      throw new KeyGenerationException("Exception whilst generating keys", e);
    }
    catch (CertificateException e) {
      throw new KeyGenerationException("Exception whilst generating keys", e);
    }
  }

  /**
   * Imports the SSL certificate to the key store.
   * 
   * @param filename
   *          String path to the SSL key certificate
   * @throws KeyGenerationException
   * @throws CertificateStoringException
   */
  public void importCert(String filename) throws KeyGenerationException, CertificateStoringException {
    try {
      // Loads the TVSKeyStore to get the path to the underlying Java KeyStore
      TVSKeyStore tvsKeystore = TVSKeyStore.getInstance(KeyStore.getDefaultType());
      tvsKeystore.load(this.randConf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PATH),
          this.randConf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PWD).toCharArray());
      KeyGeneration.importCertificates(filename,
          this.randConf.getStringParameter(ConfigFiles.RandomnessServerConfig.CA_CERT_LOCATION), this.id + SSL_SUFFIX,
          tvsKeystore.getJavaKeystorePath(), this.randConf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PWD));

    }
    catch (KeyStoreException e) {
      throw new KeyGenerationException(e);
    }
  }

  /**
   * main method to run the RGKeyGeneration algorithms
   * 
   * @param args
   *          String arrays of arguments
   */
  public static void main(String[] args) {
    CryptoUtils.initProvider();
    RGKeyGeneration kg = null;

    try {
      if (args.length < 2) {
        printUsage();
        return;
      }

      kg = new RGKeyGeneration(args[0]);

      if (args[1].equals(RGKeyGeneration.GENERATE_OPTION)) {
        if (args.length != 2) {
          printUsage();
          return;
        }

        kg.generateKeyAndCSR();
      }
      else if (args[1].equals(RGKeyGeneration.IMPORT_OPTION)) {
        if (args.length != 3) {
          printUsage();
          return;
        }

        kg.importCert(args[2]);
      }
    }
    catch (JSONIOException | KeyGenerationException | CertificateStoringException | TVSSignatureException e) {
      logger.error("Exception when running KeyGeneration", e);
    }
  }

  /**
   * Prints the usage instructions to the System.out
   */
  private static final void printUsage() {
    System.out.println("Usage of RGKeyGeneration:");
    System.out.println("RGKeyGeneration is used to generate keys, CSRs and");
    System.out.println("import the relevant certificates.");
    System.out.println("Usage:\n");
    System.out.println("RGKeyGeneration pathToConfigFile action [certificatePaths] \n");
    System.out.println("\tpathToConfigFile: path to configuration file.");
    System.out.println("\taction: Either \"gen\" or \"import\"");
    System.out.println("\t[certificatePaths]: only required if action=\"import\" should be a path to the SSLCert");
  }
}
