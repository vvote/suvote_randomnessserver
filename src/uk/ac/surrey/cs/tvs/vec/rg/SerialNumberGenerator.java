/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

import org.json.JSONException;
import org.json.JSONObject;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;

/**
 * Utility class for generating serial numbers. Longer term we might want to handle this centrally and send the file out, but for
 * the moment we will generate them on the device.
 * 
 * @author Chris Culnane
 * 
 */
public class SerialNumberGenerator {

  /**
   * Generates serial numbers, storing each one in a JSONObject which is then stored on a line in the output file.
   * 
   * @param numToGen
   *          number of serial numbers to generate
   * @param outputFile
   *          the file to write the serial numbers to
   * @param prefix
   *          the string prefix to appear before the integer part of the serial number
   * @throws IOException
   * @throws JSONException
   */
  public static void generateSerialNumbers(int numToGen, String outputFile, String prefix) throws IOException, JSONException {
    BufferedWriter bw = null;
    try {
      bw = new BufferedWriter(new FileWriter(outputFile));

      String pre = prefix + String.valueOf(MessageFields.SERIAL_NO_SEPARATOR);

      for (int i = 0; i < numToGen; i++) {
        JSONObject serial = new JSONObject();
        serial.put(MessageFields.RandomnessFile.SERIAL_NO, pre + String.valueOf(i + 1));
        bw.write(serial.toString());
        bw.newLine();
      }
    }
    finally {
      if (bw != null) {
        bw.close();
      }
    }
  }
}
