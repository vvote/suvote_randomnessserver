/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;

/**
 * Class to generate the actual randomness and encrypt it under symmetric key session key before then encrypting the symmetric key
 * under the asymmetric public key.
 * 
 * This will produce the randomness in a multi-threaded manner, utilising all available cores. To avoid issues of synchronisation
 * each core generates a share of the total number, output the data to its own file before the files are then combined. For example,
 * if we were generating 1000 ballots and had 2 cores, core 1 would generated 0 - 499, whilst core 2 generated 500 - 999.
 * 
 * This class provides a listener interface to feedback progress to the calling method.
 * 
 * @author Chris Culnane
 * 
 */
public class GenerateRandomness {

  /**
   * Logger
   */
  private static final Logger    logger            = LoggerFactory.getLogger(GenerateRandomness.class);

  /**
   * The size of each piece of randomness
   */
  private int                    bitSize;

  /**
   * Number of columns, i.e. maximum number of possible candidates on a ballot
   */
  private int                    columns;

  /**
   * The file to save the commit data into
   */
  private String                 commitFile;

  /**
   * Holds the encrypted AES key for later retrieval
   */
  private byte[]                 encryptedKey;

  /**
   * Last integer progress amount, used to increment an integer progress listener with a float based progress value
   */
  private int                    lastProgress      = 0;

  /**
   * The final output file for the randomness
   */
  private String                 outputFile;

  /**
   * The target printers public key
   */
  private PublicKey              printerPK;

  /**
   * Current progress
   */
  private float                  progress          = 0f;

  /**
   * String specifying the ElGamal Crypto Algorithm
   */
  private String                 elGamalCryptoAlg  = "ElGamal/None/NoPadding";
  /**
   * List of ProgressListeners
   */
  private List<ProgressListener> progressListeners = new ArrayList<ProgressListener>();

  /**
   * Number of rows of randomness to generate - equivalent to number of ballots
   */
  private int                    rows;

  /**
   * Constructor to initialise the GenerateRandomness class. Sets the class parameters.
   * 
   * @param printerPK
   *          target printer's public key
   * @param columns
   *          number of columns (max candidates on a ballot)
   * @param rows
   *          number of rows (number of ballots)
   * @param bitSize
   *          size of randomness in bits (256, 512, etc)
   * @param outputFile
   *          string path to destination file
   * @param commitFile
   *          string path to the file for commit data
   */
  public GenerateRandomness(PublicKey printerPK, int columns, int rows, int bitSize, String outputFile, String commitFile) {
    super();

    this.printerPK = printerPK;
    this.columns = columns;
    this.rows = rows;
    this.bitSize = bitSize;
    this.outputFile = outputFile;
    this.commitFile = commitFile;
  }

  /**
   * Adds a ProgressListener to this class
   * 
   * @param listener
   *          ProgressListener to add
   */
  public void addProgressListener(ProgressListener listener) {
    this.progressListeners.add(listener);
  }

  /**
   * Combines the list of serial numbers in one file, with the randomness in another file and outputs the combined result in a final
   * output file. This is required to handle the multi-threaded nature of the randomness generation.
   * 
   * @param inputFile
   *          File that contains the randomness
   * @param output
   *          BufferedWriter to output the combined values to
   * @param serials
   *          BufferedReader that contains a list of serial numbers
   * @throws IOException
   * @throws JSONException
   */
  private void copyFileIntoFile(File inputFile, BufferedWriter output, BufferedReader serials) throws IOException, JSONException {
    // Open input file

    BufferedReader br = null;
    try {
      br = new BufferedReader(new FileReader(inputFile));

      String line;

      // Step through each line of input
      while (((line = br.readLine()) != null)) {
        // Get next serial object and add randomness array to it
        JSONObject serial = new JSONObject(serials.readLine());
        serial.put(MessageFields.RandomnessFile.RANDOMNESS, new JSONArray(line));

        // Output row (line) to output file
        output.write(serial.toString());
        output.newLine();
      }
    }
    finally {
      if (br != null) {
        br.close();
      }
    }
  }

  /**
   * Generates the randomness value according to the parameters set in the constructor. The passed in serialFile should contain a
   * list of JSONObjects, one per line, that contain a serial number. There must be at least as many serial number objects are rows
   * being requested to be generated. This is not pre-checked, since we would have to iterate through the whole file to count the
   * number of lines. Instead we catch the situation where this requirement has not been met.
   * 
   * @param serialFile
   *          String path to serial number file
   * @throws InterruptedException
   * @throws NoSuchAlgorithmException
   * @throws IOException
   * @throws ExecutionException
   * @throws NoSuchPaddingException
   * @throws NoSuchProviderException
   * @throws InvalidKeyException
   * @throws IllegalBlockSizeException
   * @throws BadPaddingException
   * @throws JSONException
   */
  public void generate(String serialFile) throws InterruptedException, NoSuchAlgorithmException, IOException, ExecutionException,
      NoSuchPaddingException, NoSuchProviderException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException,
      JSONException {
    // Open two readers of the same file. This is required because we will be reading them line by line and need to be looking at
    // two different lines at the same time
    BufferedReader serials = null;
    BufferedReader serialsComm = null;
    BufferedWriter fos = null;
    BufferedWriter fosCommit = null;
    ExecutorService es = null;

    try {
      serials = new BufferedReader(new FileReader(serialFile));
      serialsComm = new BufferedReader(new FileReader(serialFile));
      // The printer public key is standard ElGamal. If we used EC-ElGamal we would need to worry about encoding the AES key into
      // the underlying curve - Issue #8 check with James that ElGamal/None/NoPadding is acceptable
      Cipher cipher = Cipher.getInstance(this.elGamalCryptoAlg, "BC");

      // Generate an AES Key
      KeyGenerator keyGen = KeyGenerator.getInstance("AES");
      SecureRandom krandom = new SecureRandom();
      keyGen.init(this.bitSize, krandom);
      SecretKey key = keyGen.generateKey();

      // Encrypt AES Key
      cipher.init(Cipher.ENCRYPT_MODE, this.printerPK);
      this.encryptedKey = cipher.doFinal(key.getEncoded());

      // Find out how many processors we have and how many rows per thread we will create
      int procCount = Runtime.getRuntime().availableProcessors();
      int rowsPerThread = this.rows / procCount;

      // Create a new ExecutorService
      es = Executors.newFixedThreadPool(procCount);

      // Create a list for storing the Callable RandomnessWorkers - these are the threads that will actually do the work
      List<Callable<List<File>>> randWorkers = new ArrayList<Callable<List<File>>>();

      // Create the RandomnessWorkers - this is not starting them, just creating them
      for (int i = 0; i < procCount; i++) {
        if (i == (procCount - 1)) {
          rowsPerThread = this.rows - ((i) * rowsPerThread);
        }

        // We allocate 90%/procCount of the progress value to each thread
        RandomnessWorker rw = new RandomnessWorker(key, this.columns, rowsPerThread, this.bitSize / 8, (90f / procCount), this);
        randWorkers.add(rw);
      }

      logger.info("Created {} RandomnessWorkers, will now invoke them all", procCount);
      List<Future<List<File>>> results = es.invokeAll(randWorkers);
      logger.info("All RandomnessWorkers have finished");

      logger.info("About to start combining the files");

      // Create output and commit output Writers

      fos = new BufferedWriter(new FileWriter(this.outputFile));
      fosCommit = new BufferedWriter(new FileWriter(this.commitFile));

      // 10% of our progress is allocated to this
      float tIncrement = (10f / results.size());

      // Loop through the future objects returned by the callable RandomnessWorkers. These contain a reference to the temp file that
      // hold the actual randomness
      for (Future<List<File>> future : results) {
        this.incrementProgress(tIncrement);

        // Combine output data
        this.copyFileIntoFile(future.get().get(0), fos, serials);

        // Combine commit data
        this.copyFileIntoFile(future.get().get(1), fosCommit, serialsComm);
      }

      logger.info("Finished writing files");
    }
    finally {
      if (fos != null) {
        try {
          fos.close();
        }
        catch (IOException e) {
          logger.error("Exception whilst closing outputfile", e);
        }
      }
      if (fosCommit != null) {
        try {
          fosCommit.close();
        }
        catch (IOException e) {
          logger.error("Exception whilst closing outputfile", e);
        }
      }
      if (serials != null) {
        try {
          serials.close();
        }
        catch (IOException e) {
          logger.error("Exception whilst closing outputfile", e);
        }
      }
      if (serialsComm != null) {
        try {
          serialsComm.close();
        }
        catch (IOException e) {
          logger.error("Exception whilst closing outputfile", e);
        }
      }
      if (es != null) {
        // Shutdown ExecutorService
        es.shutdown();
      }
    }
  }

  /**
   * Gets the AES session key encrypted under the printer public key
   * 
   * @return byte array containing AES session key
   */
  public byte[] getEncryptedKey() {
    return this.encryptedKey;
  }

  /**
   * Increments the progress with the float increment. This will only fire an updateProgressListeners call if the integer progress
   * will increase following the float increment.
   * 
   * @param increment
   *          float increment amount to add to the total
   */
  public void incrementProgress(float increment) {
    // Update progress total
    this.progress = this.progress + increment;

    // Round the value to an integer
    int tProgress = Math.round(this.progress);

    // Check if that results in a change to the integer progress, notify listeners if it does, if not do nothing
    if (tProgress != this.lastProgress) {
      this.lastProgress = tProgress;
      this.updateProgressListeners(this.lastProgress);
    }
  }

  /**
   * Removes a ProgressListener from this class
   * 
   * @param listener
   *          ProgressListener to remove
   */
  public void removeProgressListener(ProgressListener listener) {
    this.progressListeners.remove(listener);
  }

  /**
   * Sets the ElGamal Crypto Algorithm to use
   * 
   * @param elGamalAlg
   *          String representation of ElGamal Crypto Algorithm
   */
  public void setElGamalCryptoAlgorithm(String elGamalAlg) {
    this.elGamalCryptoAlg = elGamalAlg;
  }

  /**
   * Called to update any ProgressListeners with a new value. The value they receive is the parameter
   * 
   * @param value
   *          to pass to the ProgressListeners
   */
  private void updateProgressListeners(int value) {
    for (ProgressListener pl : this.progressListeners) {
      pl.updateProgress(value);
    }
  }
}
