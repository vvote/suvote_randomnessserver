package uk.ac.surrey.cs.tvs.vec.rg.cli;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.ui.ProgressMonitorCLI;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.vec.rg.ProcessKeyFiles;


public class RandServerCLI {

  /**
   * Default location and filename of the server config - can be overridden at the start
   */
  private static final String DEFAULT_CONFIG_PATH     = "./server_conf.json";
  /**
   * Logger
   */
  private static final Logger logger                  = LoggerFactory.getLogger(RandServerCLI.class);

  /**
   * Holds a reference to the ConfigFile for the randomness server
   */
  private ConfigFile          conf;

  /**
   * ProcessKeyFiles object that keeps a reference to the current ProcessKeyFiles object
   */
  private ProcessKeyFiles     currentPkf;

  /**
   * Schema for the config file.
   */
  private static final String CONFIG_SCHEMA           = "./schemas/configschema.json";
  
  /**
   * Name for thread that processes the keys
   */
  private static final String PROCESS_KEY_THREAD_NAME = "KeyProcessorThread";
  /**
   * Thread object that keeps a reference to the current ProcessKeyFiles thread
   */
  private Thread              currentPkfThread;
  /**
   * List of Files that have failed during uploads
   */
  private List<File>             failures          = new ArrayList<File>();
  public RandServerCLI(String configFile) throws JSONIOException, IOException, JSONException {
    super();
    this.conf = new ConfigFile(configFile, CONFIG_SCHEMA);
    CryptoUtils.initProvider();
  }

  /**
   * Processes the keys in specified keys to process folder and generates randomness, outputting progress to the CLI.
   * @return int value of the number of failures - zero means all succeeded, non-zero indicates failures 
   * @throws JSONIOException
   * @throws PeerSSLInitException
   * @throws InterruptedException
   */
  public int processKeys() throws JSONIOException, PeerSSLInitException, InterruptedException{
 // Generate Randomness
    // Create a ProgressMonitor for the files progress (i.e. progress through all files) and one for progress within an
    // individual file
    ProgressMonitorCLI pm = new ProgressMonitorCLI();
    ProgressMonitorCLI pmFile = new ProgressMonitorCLI(pm);
    pm.setChild(pmFile);

   
      logger.info("Creating new PublicKeyFiles thread");
      this.currentPkf = new ProcessKeyFiles(this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.KEYS_TO_PROCESS),
          this.conf);
      this.currentPkf.setFailuresList(this.failures);
      this.currentPkf.setInnerProgressMonitor(pmFile);
      this.currentPkf.addProgressListener(pm);
      this.currentPkfThread = new Thread(this.currentPkf, RandServerCLI.PROCESS_KEY_THREAD_NAME);
      this.currentPkfThread.start();
      this.currentPkfThread.join();
      if(!this.failures.isEmpty()){
        System.out.println("\nThe following files failed:");
        for(File f:this.failures){
          System.out.println(f.getName());
        }
        return this.failures.size();
      }else{
        System.out.println("\nAll files processed, no failures");
        return 0;
      }
    
  }
  public static void main(String[] args) {
    try {
      if (args.length == 0) {
        new RandServerCLI(RandServerCLI.DEFAULT_CONFIG_PATH).processKeys();
      }
      else if (args.length == 1) {
        int returnVal=(new RandServerCLI(args[0])).processKeys();
        System.exit(returnVal);
      }
      else {
        RandServerCLI.printUsage();
      }
    }
    catch (JSONException | JSONIOException | IOException | PeerSSLInitException  e) {
      System.out.println("An exception has occured whilst running the RandomnessServerUI");
      RandServerCLI.printUsage();
      logger.error("Exception whilst starting RandomnessServerIO", e);
    }
    catch (InterruptedException e) {
      System.out.println("Randomness Generation was interrupted");
      logger.error("Exception whilst starting RandomnessServerIO", e);
    }
  }

  /**
   * Prints the usage instructions for starting the UI
   */
  private static final void printUsage() {
    System.out.println("Usage of RandServerCLI:");
    System.out.println("RandServerCLI starts the web server that will serve");
    System.out.println("the user interface for the RandomnessServer. A configuration");
    System.out.println("file is required. If no configuration file is specified ");
    System.out.println("the default path at ./server_conf.json will be checked.");
    System.out.println("Usage:\n");
    System.out.println("RandServerCLI [pathToConfigFile] \n");
    System.out.println("\tpathToConfigFile: optional parameter to explicitly state path to configuration to use.");
  }
}
