/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import java.io.File;
import java.io.FilenameFilter;

import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.ClientConfig;

/**
 * Extension of FilenameFilter - finds all files with names that end "PublicKey.json"
 * 
 * @author Chris Culnane
 * 
 */
public class PublicKeyFilter implements FilenameFilter {

  /**
   * Tests if a specified file should be included in a file list.
   * 
   * @param file
   *          the directory in which the file was found.
   * @param name
   *          the name of the file.
   * @return <code>true</code> if and only if the name should be included in the file list; <code>false</code> otherwise.
   * 
   * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
   */
  @Override
  public boolean accept(File file, String name) {
    return name.endsWith(ClientConfig.PUBLIC_KEY_SUFFIX);
  }
}
