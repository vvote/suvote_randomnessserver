/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import java.util.zip.ZipOutputStream;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManagerFactory;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.comms.FieldResponseChecker;
import uk.ac.surrey.cs.tvs.comms.MBB;
import uk.ac.surrey.cs.tvs.comms.MBBResponse;
import uk.ac.surrey.cs.tvs.comms.exceptions.ConsensusException;
import uk.ac.surrey.cs.tvs.comms.exceptions.MBBCommunicationException;
import uk.ac.surrey.cs.tvs.comms.exceptions.PeerSSLInitException;
import uk.ac.surrey.cs.tvs.fields.messages.ClientConstants;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.KeyStoreConfig;
import uk.ac.surrey.cs.tvs.fields.messages.ConfigFiles.RandomnessServerConfig;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.FileMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageFields.JSONWBBMessage;
import uk.ac.surrey.cs.tvs.fields.messages.MessageTypes;
import uk.ac.surrey.cs.tvs.fields.messages.SystemConstants;
import uk.ac.surrey.cs.tvs.ui.ProgressListener;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.CryptoIOException;
import uk.ac.surrey.cs.tvs.utils.crypto.exceptions.TVSSignatureException;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.SignatureType;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSKeyStore;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.TVSSignature;
import uk.ac.surrey.cs.tvs.utils.crypto.signing.bls.BLSPrivateKey;
import uk.ac.surrey.cs.tvs.utils.io.ConfigFile;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;
import uk.ac.surrey.cs.tvs.utils.io.MBBConfig;
import uk.ac.surrey.cs.tvs.utils.io.ZipUtil;
import uk.ac.surrey.cs.tvs.utils.io.exceptions.JSONIOException;
import uk.ac.surrey.cs.tvs.vec.rg.setup.RGKeyGeneration;

/**
 * Runnable Class that process a set of key files in a folder, generating randomness for each key file. Each key file should be from
 * a target printer. The mode of operation is therefore to place the key files into a folder and then run this class.
 * 
 * @author Chris Culnane
 * 
 */
public class ProcessKeyFiles implements Runnable {

  /**
   * Logger
   */
  private static final Logger    logger            = LoggerFactory.getLogger(ProcessKeyFiles.class);

  /**
   * String array of acceptable cipher suites to use during SSL communication
   */
  private String[]               cipherSuites      = null;

  /**
   * Randomness generation config file
   */
  private ConfigFile             conf;

  /**
   * SSLSocketFactory to use when connecting to the MBB Peers
   */
  private SSLSocketFactory       factory;

  /**
   * List of Files that have failed during uploads
   */
  private List<File>             failures          = new ArrayList<File>();

  /**
   * Reference to GenerateRandomness object - this is required to handle setting the innerProgressMonitor
   */
  private GenerateRandomness     gr;

  /**
   * ProgressListener to monitor progress
   */
  private ProgressListener       innerProgressMon  = null;

  /**
   * Location of key file folder
   */
  private String                 keyFileFolder;

  /**
   * List of ProgressListeners to cover overall progress through the keys, not per file progress
   */
  private List<ProgressListener> progressListeners = new ArrayList<ProgressListener>();

  /**
   * TVSKeyStore that points at the system certificate store
   */
  private TVSKeyStore            certStore;

  /**
   * Constructor to create ProcessKeyFiles object.
   * 
   * @param keyFileFolder
   *          String path to top level folder containing public key files
   * @param conf
   *          ConfigFile to use for parameters and settings
   * @throws JSONIOException
   * @throws PeerSSLInitException
   */
  public ProcessKeyFiles(String keyFileFolder, ConfigFile conf) throws JSONIOException, PeerSSLInitException {
    super();
    this.keyFileFolder = keyFileFolder;
    this.conf = conf;
    CryptoUtils.initProvider();
    
    try {
      //load the certificate key store
      certStore = CryptoUtils.loadTVSKeyStore(this.conf.getStringParameter(RandomnessServerConfig.SYSTEM_CERTIFICATE_STORE));
      
      //Load SSL keys stores
      SSLContext ctx = SSLContext.getInstance("TLS");
      TVSKeyStore tvsKeyStore = TVSKeyStore.getInstance(KeyStoreConfig.KEY_STORE_TYPE);
      tvsKeyStore.load(conf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PATH),
          conf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PWD).toCharArray());
      TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
      tmf.init(tvsKeyStore.getKeyStore());
      KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
      kmf.init(tvsKeyStore.getKeyStore(), conf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PWD).toCharArray());
      ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
      this.factory = ctx.getSocketFactory();
      if (conf.hasParam(ConfigFiles.RandomnessServerConfig.CIPHER_SUITE)) {
        this.cipherSuites = new String[] { conf.getStringParameter(ConfigFiles.RandomnessServerConfig.CIPHER_SUITE) };
      }
    }
    catch (NoSuchAlgorithmException | KeyStoreException | UnrecoverableKeyException | KeyManagementException | CryptoIOException e) {
      logger.error("Error whilst initialising SSL Key and Trust Stores", e);
      throw new PeerSSLInitException("Error whilst initialising SSL Key and Trust Stores", e);
    }
  }

  /**
   * Add a ProgressListener to receive updates on the progress through the list of key files
   * 
   * @param listener
   *          ProgressListener to add
   */
  public void addProgressListener(ProgressListener listener) {
    this.progressListeners.add(listener);
  }

  /**
   * Gets the list of files that have failed during processing or uploading
   * 
   * @return List<File> of files that have failed
   */
  public List<File> getFailures() {
    return this.failures;
  }

  public void setFailuresList(List<File> failList) {
    this.failures = failList;
  }

  /**
   * Gets the SSLSocketFactory to be used when communicating with the MBB
   * 
   * @return SSLSocketFactory
   */
  public SSLSocketFactory getSSLFactory() {
    return this.factory;
  }

  /**
   * Removes a ProgressListener from the list that receives updates on the progress through the list of key files
   * 
   * @param listener
   *          ProgressListener to remove
   */
  public void removeProgressListener(ProgressListener listener) {
    this.progressListeners.remove(listener);
  }

  /**
   * Process each key file and generate randomness
   */
  @Override
  public void run() {
    // Get of folder containing keys
    File keyFilesFolder = new File(this.keyFileFolder);

    // Get list of files that match the PublicKeyFilter
    File[] keys = keyFilesFolder.listFiles(new PublicKeyFilter());

    // Calculate progress increment
    float progressIncrement = 100f / keys.length;
    float progress = 0;

    // Step through each key
    for (File key : keys) {
      try {
        // Extract the deviceID from the filename, which is of the format deviceID_PublicKey.json
        String deviceID = key.getName().substring(0, key.getName().indexOf("_"));

        // Construct an output folder for this device
        File outputFolder = new File(this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.BASE_OUTPUT_FOLDER) + deviceID
            + File.separator + this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.PEER_ID) + File.separator);
        IOUtils.checkAndMakeDirs(outputFolder);
        

        // Load the public key
        PublicKey pk = CryptoUtils.jsonToPublicKey(IOUtils.readJSONObjectFromFile(key.getAbsolutePath()));

        // Generate serial numbers and store them in the output directory in a file called serial.json
        SerialNumberGenerator.generateSerialNumbers(
            this.conf.getIntParameter(ConfigFiles.RandomnessServerConfig.ROWS_TO_GEN),
            outputFolder.getAbsolutePath() + File.separator
                + this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.SERIAL_NO_FILENAME), deviceID);

        // Create a new GenerateRandomness class with appropriate values. Output data goes into randomData.json and commitData.json
        // in the device output folder
        this.gr = new GenerateRandomness(pk, this.conf.getIntParameter(ConfigFiles.RandomnessServerConfig.COLS_TO_GEN),
            this.conf.getIntParameter(ConfigFiles.RandomnessServerConfig.ROWS_TO_GEN),
            this.conf.getIntParameter(ConfigFiles.RandomnessServerConfig.BIT_SIZE), outputFolder.getAbsolutePath() + File.separator
                + this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.RANDOM_DATA_FILENAME),
            outputFolder.getAbsolutePath() + File.separator
                + this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.COMMIT_DATA_FILENAME));

        // If we have a registered innerProgressMon set it in the GenerateRandomness
        if (this.innerProgressMon != null) {
          this.gr.addProgressListener(this.innerProgressMon);
        }

        if (this.conf.hasParam(ConfigFiles.RandomnessServerConfig.ELGAMAL_ALGORITHM)) {
          this.gr.setElGamalCryptoAlgorithm(this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.ELGAMAL_ALGORITHM));
        }
        // Generate actual randomness
        this.gr.generate(outputFolder.getAbsolutePath() + File.separator
            + this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.SERIAL_NO_FILENAME));

        // Create a JSONObject to hold the AESKey and store it to AESKey.json
        JSONObject aesKey = new JSONObject();
        aesKey.put(ClientConstants.AESKeyFile.AES_KEY, IOUtils.encodeData(EncodingType.HEX, this.gr.getEncryptedKey()));
        IOUtils.writeJSONToFile(
            aesKey,
            outputFolder.getAbsolutePath() + File.separator
                + this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.AES_KEY_FILENAME));

        // Construct a zip file in the device output folder called wbbsubmission.zip that contains the commitData.txt
        File wbbSubFile = new File(outputFolder.getAbsolutePath() + File.separator
            + this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.WBB_SUB_FILENAME));
        ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(wbbSubFile));
        MessageDigest md = MessageDigest.getInstance(ClientConstants.DEFAULT_MESSAGE_DIGEST);

        ZipUtil.addFileToZip(
            new File(outputFolder.getAbsolutePath() + File.separator
                + this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.COMMIT_DATA_FILENAME)), md, zos);
        zos.close();

        // Prepare a signature for the message to the MBB
        TVSKeyStore ks = TVSKeyStore.getInstance(KeyStoreConfig.KEY_STORE_TYPE);
        ks.load(this.conf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PATH),
            this.conf.getStringParameter(KeyStoreConfig.SIGNATURE_KEY_PWD).toCharArray());
        String peerID = this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.PEER_ID);
        BLSPrivateKey ebmKey = ks.getBLSPrivateKey(peerID + RGKeyGeneration.SIGNING_SUFFIX);

        // Create the actual signature
        TVSSignature submitSig = new TVSSignature(SignatureType.BLS, ebmKey);

        // Generate a global UUID as a serial number for this submission
        String subID = UUID.randomUUID().toString();
        submitSig.update(subID);
        submitSig.update(deviceID);
        byte[] digest = md.digest();
        String digestString = IOUtils.encodeData(EncodingType.BASE64, digest);
        submitSig.update(digestString.getBytes());

        // Construct the JSON message object
        JSONObject message = new JSONObject();
        message.put(JSONWBBMessage.TYPE, MessageTypes.MIX_RANDOM_COMMIT);
        message.put(FileMessage.SENDER_ID, this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.PEER_ID));
        message.put(FileMessage.ID, subID);
        message.put(FileMessage.SENDER_SIG, IOUtils.encodeData(EncodingType.BASE64, submitSig.sign()));
        message.put(FileMessage.DIGEST, digestString);
        message.put(FileMessage.FILESIZE, wbbSubFile.length());
        message.put(MessageFields.MixRandomCommit.PRINTER_ID, deviceID);

        MBBConfig mbbConf = new MBBConfig(this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.MBB_CONF_FILE));

        // Prepare the datatocheck value for use by the response checker
        StringBuffer dataToCheck = new StringBuffer();
        dataToCheck.append(subID);
        dataToCheck.append(this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.PEER_ID));
        dataToCheck.append(deviceID);
        dataToCheck.append(digestString);

        //Prepare a FieldResponseChecker
        FieldResponseChecker fieldResChecker = new FieldResponseChecker(certStore, SystemConstants.SIGNING_KEY_SK2_SUFFIX,
            dataToCheck.toString(), "", MessageFields.PeerResponse.COMMIT_TIME);
        
        //Get the threshold from the config
        int threshold = this.conf.getIntParameter(ConfigFiles.RandomnessServerConfig.THRESHOLD);
        
        //Send to the MBB, returning as soon as a threshold of valid responses has been received
        MBBResponse mbbResponse = MBB.sendMessageWaitForThreshold(mbbConf,
            message.toString() + MBB.FILE_SUB_SEPARATOR + wbbSubFile.getAbsolutePath(), this.factory, this.cipherSuites,
            fieldResChecker, threshold);

        logger.info("Received response, will check signature: {}",mbbResponse.getResponseArray());
        TVSSignature tvsSig = new TVSSignature(SignatureType.BLS, certStore);
        tvsSig.update(dataToCheck.toString());
        String commitTime = "";
        //Check if we have a consensus value
        if (mbbResponse.getDataItem() != null) {
          commitTime = mbbResponse.getDataItem();
        }
        else {
          // fallback to ensure that we haven't received a threshold of responses as we shutdown the timeout
          if (!MBB.checkThresholdNonErrors(mbbResponse.getResponseArray(), threshold)) {
            logger.error("Error submitting to MBB for key file {}, invalid signature", key.getAbsolutePath());
          }
          else {
            commitTime = MBB.getThresholdFieldValue(mbbResponse.getResponseArray(), MessageFields.PeerResponse.COMMIT_TIME,
                threshold);
          }
        }
        //Update the signature
        tvsSig.update(commitTime);

        //Combine the signature, re-verifying if necessary, will also check the combined signature
        JSONObject response = tvsSig.verifyAndCombine(mbbResponse.getResponseArray(), SystemConstants.SIGNING_KEY_SK2_SUFFIX,
            threshold, mbbConf.getPeers().size(), !mbbResponse.isThresholdChecked(), true);

        // *******************
       
        if (response.has(TVSSignature.COMBINED)) {
          logger.info("Response from MBB is valid");
          JSONObject outData = new JSONObject();

          outData.put(ConfigFiles.RandomnessConfig.PEER_ID,
              this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.PEER_ID));
          outData.put(ConfigFiles.RandomnessConfig.WBBSIG, response.getString(TVSSignature.COMBINED));
          outData.put(ConfigFiles.RandomnessConfig.WBBMSG, message);
          outData.put(ConfigFiles.RandomnessConfig.AES_KEY_FILE,
              this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.AES_KEY_FILENAME));
          outData.put(ConfigFiles.RandomnessConfig.RANDOMNESS_DATA_FILE,
              this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.RANDOM_DATA_FILENAME));
          outData.put(ConfigFiles.RandomnessConfig.COMMIT_DATA_FILE,
              this.conf.getStringParameter(ConfigFiles.RandomnessServerConfig.COMMIT_DATA_FILENAME));
          outData.put(MessageFields.PeerResponse.COMMIT_TIME, commitTime);
          IOUtils.writeJSONToFile(outData, outputFolder.getAbsolutePath() + File.separator
              + ConfigFiles.RandomnessConfig.RANDOMNESS_CONFIG_FILE);
          IOUtils.moveFile(key.getAbsolutePath(), this.conf.getStringParameter(RandomnessServerConfig.PROCESSED_KEYS));
        }
        else {
          logger.error("Error submitting to MBB for key file {}, invalid signature", key.getAbsolutePath());
          this.failures.add(key);
        }
        progress = progress + progressIncrement;
        this.updateProgressListeners(Math.round(progress));
      }
      catch (CryptoIOException | JSONIOException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
          | NoSuchProviderException | IllegalBlockSizeException | BadPaddingException | InterruptedException | IOException
          | ExecutionException | JSONException | KeyStoreException | TVSSignatureException | MBBCommunicationException e) {
        logger.error("Exception whilst processing key file:{}", key.getAbsolutePath(), e);
        this.failures.add(key);
      }
      catch (ConsensusException e) {
        logger.error("Consensus error when uploading data to the MBB whilst processing key file:{}", key.getAbsolutePath(), e);
        this.failures.add(key);
      }
    }
  }

  /**
   * Sets the innerProgressMonitor - this is the ProgessListener that will be attached to each instance of GenerateRandomness. We
   * create a new instance of GenerateRandomness for each key we process and therefore need to reset the listeners.
   * 
   * 
   * 
   * @param pm
   *          ProgressListener to set as the innerProgress Listener
   */
  public void setInnerProgressMonitor(ProgressListener pm) {
    this.innerProgressMon = pm;

    // Check if an instance of GenerateRandomness exists, if it does add the ProgressListener. This could occur if the front-end
    // disconnects and reconnects
    if (this.gr != null) {
      this.gr.addProgressListener(this.innerProgressMon);
    }
  }

  /**
   * Update any ProgressListeners with a new value
   * 
   * @param value
   *          to send to any ProgressListeners
   */
  private void updateProgressListeners(int value) {
    for (ProgressListener pl : this.progressListeners) {
      pl.updateProgress(value);
    }
  }
}
