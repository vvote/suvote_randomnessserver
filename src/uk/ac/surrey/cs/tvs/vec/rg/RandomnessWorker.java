/*******************************************************************************
 * Copyright (c) 2013 University of Surrey.
 * All rights reserved. This program and the accompanying materials are made
 * available under the terms of the GNU Public License v3.0 which accompanies
 * this distribution, and is available at http://www.gnu.org/licenses/gpl.html
 * 
 * Contributors:
 *     Chris Culnane - initial API and implementation
 *     Matthew Casey - review
 ******************************************************************************/
package uk.ac.surrey.cs.tvs.vec.rg;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;

import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import uk.ac.surrey.cs.tvs.fields.messages.MessageFields;
import uk.ac.surrey.cs.tvs.utils.crypto.CryptoUtils;
import uk.ac.surrey.cs.tvs.utils.crypto.EncodingType;
import uk.ac.surrey.cs.tvs.utils.io.IOUtils;

/**
 * Callable object that does the actual work of generating the randomness.
 * 
 * @author Chris Culnane
 * 
 */
public class RandomnessWorker implements Callable<List<File>> {

  /**
   * Logger
   */
  private static final Logger logger                  = LoggerFactory.getLogger(RandomnessWorker.class);

  /**
   * Extension for temp commit files
   */
  private static final String TEMP_RAND_COMMIT_EXT    = ".commit";

  /**
   * Prefix for temp commit files
   */
  private static final String TEMP_RAND_COMMIT_PREFIX = "veccommit";

  /**
   * Temp directory to create temporary files in - the temp files should be deleted on exit
   */
  public static final String  TEMP_RAND_DIR           = "./randDir/";

  /**
   * Extension for temp randomness files
   */
  private static final String TEMP_RAND_GEN_EXT       = ".rnd";

  /**
   * Prefix used for temp randomness file names
   */
  private static final String TEMP_RAND_GEN_PREFIX    = "vecrg";

  /**
   * Number of columns of randomness to generate per row
   */
  private int                 columns;

  /**
   * Reference back to GenerateRandomness so we can call progress updates
   */
  private GenerateRandomness  gr;

  /**
   * The AES SecretKey we will encrypt each piece of randomness with
   */
  private SecretKey           key;

  /**
   * Our share of the overall progress
   */
  private float               progressShare;

  /**
   * Number of bytes of randomness to generate per value
   */
  private int                 randBytes;

  /**
   * Number of rows for this randomness worker to generate
   */
  private int                 rows;

  /**
   * Constructor to create RandomnessWorker
   * 
   * @param key
   *          Secret AES Key used to encrypt each piece of random data
   * @param columns
   *          the number of columns to generate
   * @param rows
   *          the number of rows to generate
   * @param randBytes
   *          the number of bytes of randomness to generate per value
   * @param progressShare
   *          the share of overall progress this thread has
   * @param gr
   *          reference to calling GenerateRandomness to pass progress updates back to
   */
  public RandomnessWorker(SecretKey key, int columns, int rows, int randBytes, float progressShare, GenerateRandomness gr) {
    super();

    this.columns = columns;
    this.rows = rows;
    this.randBytes = randBytes;
    this.key = key;
    this.progressShare = progressShare;
    this.gr = gr;
  }

  /**
   * Creates the randomness, encrypts it and outputs it to temp files
   * 
   * @return List<File> containing the two temp files with the necessary data
   * @throws Exception
   */
  @Override
  public List<File> call() throws Exception {
    // This is just a temp directory for storing files in, it could be anything
    File baseDir = new File(RandomnessWorker.TEMP_RAND_DIR);
    IOUtils.checkAndMakeDirs(baseDir);
    

    // Create a temp file for the output data (randomness and commit) - these will be combined with the output from other threads
    // later
    File tempOut = File.createTempFile(RandomnessWorker.TEMP_RAND_GEN_PREFIX, RandomnessWorker.TEMP_RAND_GEN_EXT, baseDir);
    File tempCommit = File.createTempFile(RandomnessWorker.TEMP_RAND_COMMIT_PREFIX, RandomnessWorker.TEMP_RAND_COMMIT_EXT, baseDir);

    // Set them to be deleted on exit
    tempOut.deleteOnExit();
    tempCommit.deleteOnExit();

    // Create a new source of randomness
    SecureRandom sr = new SecureRandom();

    // Prepare the AES cipher - Issue #5 Check with James AES/ECB/NoPadding mode is acceptable
    Cipher cipher = Cipher.getInstance("AES/ECB/NoPadding");
    cipher.init(Cipher.ENCRYPT_MODE, this.key);

    // Prepare the output files

    BufferedWriter bw = null;
    BufferedWriter bwCommit = null;

    try {
      bw = new BufferedWriter(new FileWriter(tempOut));
      bwCommit = new BufferedWriter(new FileWriter(tempCommit));
      // Calculate the increment
      float increment = this.progressShare / this.rows;

      // Step through each row
      for (int i = 0; i < this.rows; i++) {
        // Create output JSON objects
        JSONArray row = new JSONArray();
        JSONArray rowCommit = new JSONArray();

        // Step through each column
        for (int j = 0; j < this.columns; j++) {
          JSONObject obj = new JSONObject();

          // Create new buffer for randomness
          byte[] buffer = new byte[this.randBytes];
          sr.nextBytes(buffer);

          // Encode encrypted randomness value
          obj.put(MessageFields.RandomnessFile.RANDOM_VALUE, IOUtils.encodeData(EncodingType.HEX, cipher.doFinal(buffer)));

          // Create commit randomness value
          byte[] comm = new byte[this.randBytes];
          sr.nextBytes(comm);

          // Output data
          obj.put(MessageFields.RandomnessFile.RANDOM_VALUE_COMMIT, IOUtils.encodeData(EncodingType.HEX, cipher.doFinal(comm)));
          row.put(obj);

          rowCommit.put(IOUtils.encodeData(EncodingType.HEX, CryptoUtils.generateCommitment(buffer, comm)));
        }

        bw.write(row.toString());
        bw.newLine();
        bwCommit.write(rowCommit.toString());
        bwCommit.newLine();

        this.gr.incrementProgress(increment);
      }
    }
    finally {
      if (bw != null) {
        try {
          bw.close();
        }
        catch (IOException e) {
          logger.error("Exception whilst closing temp file", e);
        }
      }
      if (bwCommit != null) {
        try {
          bwCommit.close();
        }
        catch (IOException e) {
          logger.error("Exception whilst closing temp file", e);
        }
      }

    }

    // Prepare return List of files
    List<File> files = new ArrayList<File>();
    files.add(tempOut);
    files.add(tempCommit);
    return files;
  }
}
